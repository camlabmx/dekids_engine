from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
from system import exceptionCtrl

def headerInterceptor(header,permissions):
    try:
        #First check Authorization
        token=None
        if 'Authorization' in header:
            token = db.tokens.find_one({'token':header['Authorization']})
        if token:
            #Then get user object
            user = db.users.find_one({'username':token['username']},{'_id':1,'username':1,'cell':1,'phone':1,'email':1,'name':1,'last_name':1,'address':1,'role':1,'permissions':1})
            if not user:
                # Try fb user
                user = db.users.find_one({'fb_id':token['username']},{'_id':1,'username':1,'cell':1,'phone':1,'email':1,'name':1,'last_name':1,'address':1,'role':1,'permissions':1})
            user['_id']=str(user['_id'])
            if user:
                #Check permissions
                if user['role']=='root':
                    return True,user
                else:
                    has_permission=True
                    if permissions:
                        for permission in permissions:
                            if not permission in user['permissions']:
                                has_permission=False
                    else:
                        #Just token verification
                        pass
                    if has_permission:
                        return True,user
                    else:
                        return False,None
            else:
                return False,None
        else:
            return False,None
        return True,user
    except Exception as e:
        print("ERROR:"+str(e))
        exceptionCtrl.StoreException('middleware/interceptor/headerInterceptor',{},str(e))
        return False,None
