from bson import ObjectId
from database import db
import datetime

def StoreException(section,data,exception):
    try:
        #Store the exception control object
        exceptionObject = {}
        exceptionObject['section']=section
        exceptionObject['data']=data
        exceptionObject['exception']=exception
        exceptionObject['datetime'] = str(datetime.datetime.now())
        exceptionObject['status'] = 'new'
        db.exception.insert(exceptionObject)
    except Exception as e:
        print("Store exception crashed :(")
        print(str(e))
