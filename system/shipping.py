from database import db
from system import exceptionCtrl
import datetime as dt
import requests
import json

def ShippifyService():
    try:
        # shipping_list = db.shipping.find({'delivery':dt.datetime.strftime(dt.datetime.now(),"%d/%m/%Y")})
        shipping_list = db.shipping.find({'status':'pending'})
        deliveries = []
        pickup = {
          "contact": {
            "name": "Usuario Pruebas",
            "email": "jcamarena@camlab.mx",
            "phonenumber": "5510825870"
          },
          "location": {
            "address": "Extremadura 153, Insurgentes Mixcoac, Benito Juárez, 03290, CDMX",
            "instructions": "Interior 4"
          }
        }
        for ship in shipping_list:
            dropoff = {}
            dropoff['contact']={}
            dropoff['location']={}
            dropoff['contact']['name']=ship['user']['name']+" "+ship['user']['last_name']
            dropoff['contact']['email']=ship['user']['email']
            dropoff['contact']['phonenumber']=ship['address']['phone']
            dropoff['location']['address']=ship['address']['street']+","+ship['address']['street2']+","+ship['address']['street3']+","+ship['address']['zip']
            dropoff['location']['instructions']=ship['address']['reference']
            dropoff['location']['lat']=ship['address']['coords']['lat']
            dropoff['location']['lng']=ship['address']['coords']['lng']
            packages=[]
            for item in ship['ship']:
                packages.append({"id":item['product'],"name":"Colágeno Hidrolizado","size":"xs","qty":item['quantity']})
            deliveries.append({"pickup":pickup,"dropoff":dropoff,"packages":packages,"referenceId":str(ship['_id'])})
        headers = {"Authorization":"Basic ajh6aDI1OHlvdG9rMjFhbjk2YjViNm5ydHZzaGJiajRpOmo4emgyNXp4OXZmZm1lODdmcGc0NHBsZGk=","Content-type":"application/json"}
        # import ipdb; ipdb.set_trace()
        res = requests.post("https://api.shippify.co/v1/deliveries/",headers=headers, json = {"deliveries":deliveries})

    except Exception as e:
        print(str(e))
        exceptionCtrl.StoreException('shipping.ShippifyService',{},str(e))
