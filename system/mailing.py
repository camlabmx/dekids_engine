from database import db
from system import exceptionCtrl
import datetime as dt
from base import mailService

def detoxDailyMail():
    #get daily mail
    mailing_list = db.mailing.find({'send_on':dt.datetime.strftime(dt.datetime.now(),"%d/%m/%Y")})
    for mail_item in mailing_list:
        mail_class=None
        if mail_item['mail']=='beforedetox':
            mail_class = DayBeforeMail()
        if mail_item['mail']=='detox1':
            mail_class = Detox1Mail()
        if mail_item['mail']=='detox2':
            mail_class = Detox2Mail()
        if mail_item['mail']=='detox3':
            mail_class = Detox3Mail()
        if mail_item['mail']=='detoxend':
            mail_class = DetoxEndMail()
        if mail_item['mail']=='detox4':
            mail_class = Detox4Mail()
        if mail_item['mail']=='detox5':
            mail_class = Detox5Mail()
        if mail_item['mail']=='detox6':
            mail_class = Detox6Mail()
        if mail_item['mail']=='birthday':
            mail_class = BirthdayMail(mail_item['code'])
        if mail_item['mail']=='teareminder':
            pass
        if mail_item['mail']=='superfoodreminder':
            pass
        if mail_item['mail']=='detox3reminder':
            pass
        if mail_item['mail']=='detox4reminder':
            pass
        if mail_item['mail']=='detox5reminder':
            pass
        if mail_item['mail']=='detox7reminder':
            pass
        if mail_item['mail']=='superfoodrecipes':
            mail_class = SuperfoodRecipesMail()
        mailService.sendMail(mail_item['to'],mail_class.subject,mail_class.body)

class DayBeforeMail:
    subject = '¡Mañana empiezas! ¿Estás listo?'
    body = '''
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Elixir - Mail</title>
    	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600" rel="stylesheet" type="text/css" />
    </head>
    <body>

    <table align="center" bgcolor="#FFF" border="0" cellpadding="0" cellspacing="0" width="600">
    	<tbody>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540"><a href="https://www.elixirdetox.com/"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A206%2C%22height%22%3A44%2C%22alt_text%22%3A%22elixirdetox%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/logo_final_tm.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="elixirdetox" height="44" src="https://www.elixirdetox.com/assets/logo_final_tm.png" style="width: 206px; height: 44px;" width="206" /></span></a></td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<h2 style="font-family: 'Titillium Web', sans-serif; font-size: 33px; font-weight: 600; color: #9dcf47;">&iexcl;Ma&ntilde;ana empiezas! &iquest;Est&aacute;s listo?</h2>
    									</td>
    								</tr>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">&iexcl;Estamos muy emocionados de que te hayas comprometido a regalarle un respiro de salud y bienestar a tu cuerpo para que te sientas mucho m&aacute;s pleno y feliz en tu d&iacute;a a d&iacute;a!</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Durante el tiempo que dure tu proceso de desintoxicaci&oacute;n, te estaremos guiando y apoyando para que le puedas sacar el mayor provecho a &eacute;ste y logres eliminar toda la toxicidad que tu cuerpo ha ido acumulando a lo largo del tiempo.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Para empezar te vamos a recordar cual es el protocolo que tendr&aacute;s que seguir por los pr&oacute;ximos d&iacute;as.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">&iquest;C&oacute;mo hacer mi Detox?</p>

    									<p>&nbsp;</p>

    									<ul style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">
    										<li>Vas a tomar durante el d&iacute;a los 6 jugos que te enviamos la noche anterior.</li>
    										<li>Para comenzar el d&iacute;a empieza con el &ldquo;Shot de jengibre&rdquo; 5 minutos antes de tu primer jugo.</li>
    										<li>Recuerda que los jugos los debes tomar en un lapso de entre 2:30 a 3:00 horas y que debes dejar pasar un m&iacute;nimo de 12 horas entre el &uacute;ltimo jugo de la noche y el primer jugo del d&iacute;a siguiente.</li>
    										<li>TIP: Si por tus horarios se te complica tomarlos en este lapso, no pasa nada, adec&uacute;alos para que te sea c&oacute;modo tomarlos con la misma frecuencia.</li>
    										<li>En tu paquete de jugos vas a ver un sobrecito con semillas de Ch&iacute;a, &eacute;stas deben de ir en cualquiera de los jugos del d&iacute;a, nosotros recomendamos que la vac&iacute;es en el segundo.</li>
    									</ul>

    									<p>&nbsp;</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Tips para sacarle mayor provecho a tu Detox</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">1. Recuerda tomar suficiente agua natural durante el d&iacute;a. Sabemos que vas a estar en una dieta de s&oacute;lo jugos y es f&aacute;cil asumir que con estos es suficiente l&iacute;quido para tu cuerpo, pero para que &eacute;ste pueda eliminar adecuadamente la toxicidad acumulada necesita estar bien hidratado. Te sugerimos que complementes tu programa acompa&ntilde;ado de AGUA ALCALINA, que a la par de hidratar, ayuda a eliminar desechos t&oacute;xicos, atrapa radicales libres y es antioxidante. La encuentras en nuestra tienda en l&iacute;nea elixirdetox.com y te la podemos llevar a domicilio con alguna de las entrega de tu programa.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">2. Toma agua natural caliente, como si fuera para t&eacute;, en peque&ntilde;os tragos a lo largo del d&iacute;a. &Eacute;sta promueve el movimiento de tu sistema linf&aacute;tico, dren&aacute;ndolo y permitiendo que libere con mayor facilidad la toxicidad acumulada.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">3. &iexcl;R-E-L-&Aacute;-J&Aacute;-T-E! Entrar en un proceso de desintoxicaci&oacute;n hace que tu cuerpo tenga que trabajar de una manera muy diferente a lo que est&aacute; acostumbrado. Si tu cuerpo se siente estresado, va a interpretar la desintoxicaci&oacute;n como una amenaza y se va a aferrar a la grasa como arma de defensa impidiendo la correcta eliminaci&oacute;n de &eacute;sta, que es donde acumulamos la toxicidad. As&iacute; que intenta estar lo m&aacute;s relajado posible.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">4. &iexcl;Ll&eacute;vatela leve con el ejercicio! Si est&aacute;s acostumbrado a hacer ejercicio de una manera muy intensa, te sugerimos que durante los d&iacute;as que dure tu desintoxicaci&oacute;n le bajes a la intensidad porque si no puedes bloquear la capacidad de tu cuerpo para desintoxicarse. Lo ideal es que durante este proceso tu cuerpo enfoque la mayor energ&iacute;a posible a liberarse de aquello que no le sirve y si lo saturas con ejercicio extremo vas a impedir que lo haga.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">5. Al&eacute;jate para siempre de la nicotina, la cafe&iacute;na, el tabaco, el alcohol, el trigo y/o gluten, embutidos, edulcorantes en general (naturales o artificiales), las harinas procesadas y refinadas, los az&uacute;cares refinados, los l&aacute;cteos y el huevo. En particular este tipo de alimentos generan toxicidad en el cuerpo as&iacute; que ev&iacute;talos a toda costa.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">6. Cada quien experimenta su proceso de desintoxicaci&oacute;n de una forma muy diferente, as&iacute; que si a pesar de tomar los jugos cada 2:30 horas tienes mucha hambre, es preferible que comas algo s&oacute;lido de la &ldquo;Lista Sugerida de Alimentos&rdquo; a que te mueras de hambre, ya que esto lo interpreta tu cuerpo como estr&eacute;s lo cual bloquea tu capacidad de desintoxicaci&oacute;n. S&eacute; paciente contigo, y &iexcl;Da tu mayor esfuerzo pero sin llevarte al l&iacute;mite!</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">7. &iexcl;Duerme por lo menos 8 horas! Durante la noche es cuando nuestros &oacute;rganos entran en un proceso de regeneraci&oacute;n y cuando enfocan su energ&iacute;a en &ldquo;limpiar&rdquo;. As&iacute; que aseg&uacute;rate por los pr&oacute;ximos d&iacute;as dormir suficiente para que tu cuerpo pueda sacarle el mayor provecho a esta desintoxicaci&oacute;n.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">No olvides visitar nuestra tienda en l&iacute;nea, en elixirdetox.com ah&iacute; encontrar&aacute;s muchos m&aacute;s productos saludables que pueden formar parte de tu alimentaci&oacute;n diaria y te ayudaran a mantenerte sano y sin dejar que tu cuerpo llegue a los altos niveles de intoxicaci&oacute;n.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 23px; color: #636363; font-weight: 600; line-height: 1.25em;">&iexcl;&Aacute;nimo y Suerte ma&ntilde;ana!</p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #636363; margin: 0;"><a href="mailto:info@elixirdetox.com" style="display: block; color: #9dcf47; text-decoration: none;">info@elixirdetox.com</a></p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff; margin: 0;">01 800 00DETOX(33869)</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff;">S&iacute;guenos en:</p>

    									<p><a href="https://www.facebook.com/ElixirDetoxMexico" style="color: transparent; display: inline-block;" target="_blank"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="https://twitter.com/elixirdetox" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="http://instagram.com/elixirdetox/" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="http://www.pinterest.com/ElixirDetoxMx/" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="https://www.youtube.com/user/elixirdetoxmexico" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png" style="width: 25px; height: 28px;" width="25" /></span> </a></p>
    									</td>
    									<td>
    									<p style="margin: 0; text-align: right;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A107%2C%22height%22%3A114%2C%22alt_text%22%3A%22in%20juice%20we%20trust%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/slogan_footer.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="in juice we trust" height="114" src="https://www.elixirdetox.com/assets/slogan_footer.png" style="width: 107px; height: 114px;" width="107" /></span></p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    </body>
    </html>

    '''

class Detox1Mail:
    subject = '¿LISTO PARA COMENZAR CON TU DETOX?'
    body='''
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Elixir - Mail</title>
    	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600" rel="stylesheet" type="text/css" />
    </head>
    <body>

    <table align="center" bgcolor="#FFF" border="0" cellpadding="0" cellspacing="0" width="600">
    	<tbody>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540"><a href="https://www.elixirdetox.com/"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A206%2C%22height%22%3A44%2C%22alt_text%22%3A%22elixirdetox%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/logo_final_tm.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="elixirdetox" height="44" src="https://www.elixirdetox.com/assets/logo_final_tm.png" style="width: 206px; height: 44px;" width="206" /></span></a></td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<h2 style="font-family: 'Titillium Web', sans-serif; font-size: 33px; font-weight: 600; color: #9dcf47;">&iexcl;&Aacute;nimo, hoy es tu primer d&iacute;a&iexcl;</h2>
    									</td>
    								</tr>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Estamos seguros de que una vez que termines te vas a sentir tan revitalizado que lo vas a querer hacer de nuevo.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Aseg&uacute;rate de haber le&iacute;do la Gu&iacute;a que te mandamos con tu hielera en la primera entrega del programa ya que ah&iacute; encontrar&aacute;s valiosa informaci&oacute;n que contestar&aacute; la mayor&iacute;a de las preguntas que pudieras tener.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Hoy es el primer d&iacute;a y es cuando los s&iacute;ntomas de desintoxicaci&oacute;n se hacen sentir m&aacute;s fuerte, en especial si consumes con frecuencia substancias como cafe&iacute;na o nicotina. As&iacute; que es muy normal que tengas dolor de cabeza, o que sientas el cuerpo cortado como si te fuera a dar gripa.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Otros de los s&iacute;ntomas comunes que puedes sentir al acabar el dia de hoy son:</p>

    									<p>&nbsp;</p>

    									<ul style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">
    										<li>Dolor de cabeza.</li>
    										<li>Falta de energ&iacute;a.</li>
    										<li>N&aacute;useas.</li>
    										<li>Fatiga.</li>
    										<li>Ansiedad.</li>
    										<li>Estre&ntilde;imiento / Diarrea.</li>
    										<li>Erupciones en la piel.</li>
    										<li>Boca seca.</li>
    										<li>Dolor muscular.</li>
    										<li>Insomnio.</li>
    										<li>Exceso de energ&iacute;a.</li>
    									</ul>

    									<p>&nbsp;</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Si sientes alguno de ellos, felic&iacute;tate porque es una se&ntilde;al de que vas por buen camino y simplemente trata de atravesar los s&iacute;ntomas sin prestarles mucha atenci&oacute;n.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">No olvides tomar mucha agua, ESA ES LA CLAVE EN ESTE PROCESO. Y si quieres hacer tu experiencia a&uacute;n m&aacute;s completa, te sugerimos que complementes tu programa acompa&ntilde;ado de AGUA ALCALINA, que a la par de hidratar, ayuda a eliminar desechos t&oacute;xicos, atrapa radicales libres y es antioxidante. La encuentras en nuestra tienda en l&iacute;nea elixirdetox.com y te la podemos llevar a domicilio con la entrega de ma&ntilde;ana</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Si tus s&iacute;ntomas son muy intensos por favor escr&iacute;benos a <a href="mailto:info@elixirdetox.com" style="font-family: 'Titillium Web', sans-serif; font-size: 13px; font-weight: 600; color: #9dcf47;">info@elixirdetox.com</a> para que nuestra Health Coach pueda ver tu caso particular y te pueda dar sugerencias espec&iacute;ficas que te hagan sentir m&aacute;s c&oacute;modo.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 23px; color: #636363; font-weight: 600; line-height: 1.25em;">&iexcl;Vas por buen camino!</p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #636363; margin: 0;"><a href="mailto:info@elixirdetox.com" style="display: block; color: #9dcf47; text-decoration: none;">info@elixirdetox.com</a></p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff; margin: 0;">01 800 00DETOX(33869)</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff;">S&iacute;guenos en:</p>

    									<p><a href="https://www.facebook.com/ElixirDetoxMexico" style="color: transparent; display: inline-block;" target="_blank"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="https://twitter.com/elixirdetox" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="http://instagram.com/elixirdetox/" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="http://www.pinterest.com/ElixirDetoxMx/" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="https://www.youtube.com/user/elixirdetoxmexico" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png" style="width: 25px; height: 28px;" width="25" /></span> </a></p>
    									</td>
    									<td>
    									<p style="margin: 0; text-align: right;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A107%2C%22height%22%3A114%2C%22alt_text%22%3A%22in%20juice%20we%20trust%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/slogan_footer.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="in juice we trust" height="114" src="https://www.elixirdetox.com/assets/slogan_footer.png" style="width: 107px; height: 114px;" width="107" /></span></p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    </body>
    </html>

    '''

class Detox2Mail:
    subject = '¿QUÉ TAL AYER, CUMPLISTE EL RETO?'
    body='''
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Elixir - Mail</title>
    	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600" rel="stylesheet" type="text/css" />
    </head>
    <body>

    <table align="center" bgcolor="#FFF" border="0" cellpadding="0" cellspacing="0" width="600">
    	<tbody>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540"><a href="https://www.elixirdetox.com/"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22elixirdetox%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/logo_final_tm.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="elixirdetox" src="https://www.elixirdetox.com/assets/logo_final_tm.png" style="" /></span></a></td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<h2 style="font-family: 'Titillium Web', sans-serif; font-size: 33px; font-weight: 600; color: #9dcf47;">&iexcl;Bienvenido al d&iacute;a 2&iexcl;</h2>
    									</td>
    								</tr>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">&iquest;C&oacute;mo te sientes? &iquest;sigues animado?</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Las primeras 36 horas son siempre las m&aacute;s pesadas &iexcl;Aguanta!</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Hoy a la mitad del d&iacute;a las cosas van a ir mejorando poco a poco y los efectos positivos de los que todo el mundo habla empiezan a aparecer.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">&iquest;Quieres saber qu&eacute; est&aacute; pasando en tu cuerpo? &iexcl;Se esta limpiando! En este punto del programa lo que empieza a suceder es que probablemente experimentes algo de estre&ntilde;imiento/ diarrea como s&iacute;ntoma de la limpieza que est&aacute;s teniendo.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Aunque esto es parte del proceso, es muy importante que tu sistema digestivo pueda eliminar adecuadamente la toxicidad que ha acumulado, as&iacute; que si tu caso es el anterior, te sugiero que hagas lo siguiente para asegurarnos que tu cuerpo deseche todo lo que ya no necesita:</p>

    									<p>&nbsp;</p>

    									<ul style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">
    										<li>Toma suficiente agua natural durante el d&iacute;a (2 litros es una muy buena medida de preferencia toma agua alcalina), y si &eacute;sta puede ser tibia mucho mejor, ya que as&iacute; estar&aacute;s promoviendo el trabajo de tu sistema digestivo.</li>
    										<li>Toma una cucharada de linaza molida disuelta en un vaso con agua de lim&oacute;n en la ma&ntilde;ana en ayunas (antes del Shot de jengibre) y otro en la noche antes de dormirte (despu&eacute;s de tu &uacute;ltimo jugo del d&iacute;a)</li>
    										<li>Haz una caminata diaria de 20 mins.</li>
    									</ul>

    									<p>&nbsp;</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Si tienes m&aacute;s dudas acerca del tema y tus s&iacute;ntomas de estre&ntilde;imiento no mejoran, escr&iacute;benos a <a href="mailto:info@elixirdetox.com" style="font-family: 'Titillium Web', sans-serif; font-size: 13px; font-weight: 600; color: #9dcf47;">info@elixirdetox.com</a> para que nuestra Health Coach te pueda dar algunas otras sugerencias personalizadas.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 23px; color: #636363; font-weight: 600; line-height: 1.25em;">&iexcl;Vas por buen camino, sigue con la misma energ&iacute;a para ma&ntilde;ana!</p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #636363; margin: 0;"><a href="mailto:info@elixirdetox.com" style="display: block; color: #9dcf47; text-decoration: none;">info@elixirdetox.com</a></p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff; margin: 0;">01 800 00DETOX(33869)</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff;">S&iacute;guenos en:</p>

    									<p><a href="https://www.facebook.com/ElixirDetoxMexico" style="color: transparent; display: inline-block;" target="_blank"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="https://twitter.com/elixirdetox" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="http://instagram.com/elixirdetox/" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="http://www.pinterest.com/ElixirDetoxMx/" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="https://www.youtube.com/user/elixirdetoxmexico" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png" style="width: 25px; height: 28px;" width="25" /></span> </a></p>
    									</td>
    									<td>
    									<p style="margin: 0; text-align: right;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22in%20juice%20we%20trust%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/slogan_footer.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="in juice we trust" src="https://www.elixirdetox.com/assets/slogan_footer.png" /></span></p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    </body>
    </html>

    '''

class Detox3Mail:
    subject='¿HOY AMANECISTE MEJOR VERDAD?'
    body='''
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Elixir - Mail</title>
    	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600" rel="stylesheet" type="text/css" />
    </head>
    <body>

    <table align="center" bgcolor="#FFF" border="0" cellpadding="0" cellspacing="0" width="600">
    	<tbody>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A206%2C%22height%22%3A44%2C%22alt_text%22%3A%22elixirdetox%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/logo_final_tm.png%22%2C%22link%22%3A%22https%3A//www.elixirdetox.com/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.elixirdetox.com/"><img alt="elixirdetox" height="44" src="https://www.elixirdetox.com/assets/logo_final_tm.png" style="width: 206px; height: 44px;" width="206" /></a></span></td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<h2 style="font-family: 'Titillium Web', sans-serif; font-size: 33px; font-weight: 600; color: #9dcf47;">&iexcl;Lleg&oacute; el d&iacute;a 3&iexcl;</h2>
    									</td>
    								</tr>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">&iquest;Empiezas a notar como tu cuerpo se siente diferente? Puede que te haya dado hambre m&aacute;s temprano o que no te haya dado tiempo de tener hambre si quiera con todos los jugos que est&aacute;s tomando. Presta atenci&oacute;n a como te sientes porque durante estos procesos es muy valioso que escuches lo que tu cuerpo tiene que decirte para que no lo vuelvas a someter a comidas que le hagan da&ntilde;o y as&iacute; puedas mantener los beneficios de la desintoxicaci&oacute;n a largo plazo.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Una vez que empezamos a liberar la toxicidad que se acumula en nuestro cuerpo es com&uacute;n que notemos cambios en nuestro humor y estos son muy normales. De acuerdo con la Ayurveda la toxicidad emocional se acumula en la grasa as&iacute; que durante la desintoxicaci&oacute;n no s&oacute;lo te estar&aacute;s librando de toxinas que hayas consumido en tu comida o a trav&eacute;s del medio ambiente, sino tambi&eacute;n de patrones emocionales negativos.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">As&iacute; que si te sientes un poco m&aacute;s &ldquo;sensible&rdquo; que de costumbre no te espantes, felic&iacute;tate porque eso quiere decir que a nivel emocional tambi&eacute;n est&aacute;s haciendo una limpieza.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 23px; color: #636363; font-weight: 600; line-height: 1.25em;">&iexcl;Vas por buen camino&iexcl;</p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #636363; margin: 0;"><a href="mailto:info@elixirdetox.com" style="display: block; color: #9dcf47; text-decoration: none;">info@elixirdetox.com</a></p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff; margin: 0;">01 800 00DETOX(33869)</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff;">S&iacute;guenos en:</p>

    									<p><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png%22%2C%22link%22%3A%22https%3A//www.facebook.com/ElixirDetoxMexico%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.facebook.com/ElixirDetoxMexico" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png%22%2C%22link%22%3A%22https%3A//twitter.com/elixirdetox%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://twitter.com/elixirdetox" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png%22%2C%22link%22%3A%22http%3A//instagram.com/elixirdetox/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="http://instagram.com/elixirdetox/" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png%22%2C%22link%22%3A%22http%3A//www.pinterest.com/ElixirDetoxMx/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="http://www.pinterest.com/ElixirDetoxMx/" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png%22%2C%22link%22%3A%22https%3A//www.youtube.com/user/elixirdetoxmexico%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.youtube.com/user/elixirdetoxmexico" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png" style="width: 25px; height: 28px;" width="25" /></a></span></p>
    									</td>
    									<td>
    									<p style="margin: 0; text-align: right;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22in%20juice%20we%20trust%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/slogan_footer.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="in juice we trust" src="https://www.elixirdetox.com/assets/slogan_footer.png" /></span></p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    </body>
    </html>

        '''

class DetoxEndMail:
    subject = '¡TERMINASTE! ¡FELICITATE!'
    body='''
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Elixir - Mail</title>
    	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600" rel="stylesheet" type="text/css" />
    </head>
    <body>
    <p style="text-align: center;">&lt;%body%&gt;</p>

    <table align="center" bgcolor="#FFF" border="0" cellpadding="0" cellspacing="0" width="600">
    	<tbody>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540"><a href="https://www.elixirdetox.com/"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A206%2C%22height%22%3A44%2C%22alt_text%22%3A%22elixirdetox%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/logo_final_tm.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="elixirdetox" height="44" src="https://www.elixirdetox.com/assets/logo_final_tm.png" style="width: 206px; height: 44px;" width="206" /></span></a></td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<h2 style="font-family: 'Titillium Web', sans-serif; font-size: 33px; font-weight: 600; color: #9dcf47;">&iexcl;Terminaste! &iexcl;Felicidades!</h2>
    									</td>
    								</tr>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 23px; font-weight: 600; color: #636363; margin: 0; line-height: 1.25em;">&iexcl;Felicidades, has concluido tu desintoxicaci&oacute;n!</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Seguramente para este momento te sientes muy orgulloso de haberlo hecho y de haberte regalado la oportunidad de limpiar tu cuerpo a fondo.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Para que te sientas m&aacute;s motivado de lo que ya est&aacute;s, te recordamos algunos de los beneficios que acabas de obtener despu&eacute;s de estos d&iacute;as:</p>

    									<p>&nbsp;</p>

    									<ul style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">
    										<li>Disminuci&oacute;n en inflamaci&oacute;n de c&eacute;lulas y del cuerpo en general.</li>
    										<li>Disminuci&oacute;n en adicci&oacute;n a la cafe&iacute;na, el az&uacute;car y la comida chatarra.</li>
    										<li>P&eacute;rdida de peso.</li>
    										<li>Limpieza de la piel.</li>
    										<li>Claridad mental.</li>
    										<li>Sue&ntilde;o reparador.</li>
    										<li>Disminuci&oacute;n de dolores musculares.</li>
    										<li>Prevenci&oacute;n o mejora de los s&iacute;ntomas de algunas enfermedades como infecciones cut&aacute;neas y gripa.</li>
    									</ul>

    									<p>&nbsp;</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Ha sido un placer acompa&ntilde;arte a trav&eacute;s de este maravilloso proceso de desintoxicaci&oacute;n, esperamos que te sientas renovado y ligero y que cuando vuelvas a pensar en una desintoxicaci&oacute;n tengas a Elixir siempre en mente.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Si te interesa continuar con este nuevo estilo de vida y llevar la desintoxicaci&oacute;n al siguiente nivel, te sugerimos que le escribas a nuestra Health Coach <a href="mailto:hc@elixirdetox.com" style="font-family: 'Titillium Web', sans-serif; font-size: 13px; font-weight: 600; color: #9dcf47;">hc@elixirdetox.com</a> para que te platique de los planes Post-Detox que te puede ofrecer.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">No olvides visitar nuestra tienda en l&iacute;nea, en elixirdetox.com ah&iacute; encontrar&aacute;s muchos m&aacute;s productos saludables que pueden formar parte de tu alimentaci&oacute;n diaria y te ayudar&aacute;n a mantenerte sano y sin dejar que tu cuerpo llegue a los altos niveles de intoxicaci&oacute;n.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">&iexcl;Esperamos volver a trabajar contigo y no olvides, la limpieza que se logra con detox de 3 d&iacute;as la puedes volver a repetir una vez al meses!.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 16px; color: #636363; font-weight: 600; line-height: 1.25em;">&iexcl;Muchas gracias por confiar en nosotros!.</p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #636363; margin: 0;"><a href="mailto:info@elixirdetox.com" style="display: block; color: #9dcf47; text-decoration: none;">info@elixirdetox.com</a></p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff; margin: 0;">01 800 00DETOX(33869)</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff;">S&iacute;guenos en:</p>

    									<p><a href="https://www.facebook.com/ElixirDetoxMexico" style="color: transparent; display: inline-block;" target="_blank"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="https://twitter.com/elixirdetox" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="http://instagram.com/elixirdetox/" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="http://www.pinterest.com/ElixirDetoxMx/" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="https://www.youtube.com/user/elixirdetoxmexico" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png" style="width: 25px; height: 28px;" width="25" /></span> </a></p>
    									</td>
    									<td>
    									<p style="margin: 0; text-align: right;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A107%2C%22height%22%3A114%2C%22alt_text%22%3A%22in%20juice%20we%20trust%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/slogan_footer.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="in juice we trust" height="114" src="https://www.elixirdetox.com/assets/slogan_footer.png" style="width: 107px; height: 114px;" width="107" /></span></p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    </body>
    </html>

        '''

class Detox4Mail:
    subject='¿COMO TE SIENTES HOY?'
    body='''
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Elixir - Mail</title>
    	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600" rel="stylesheet" type="text/css" />
    </head>
    <body>

    <table align="center" bgcolor="#FFF" border="0" cellpadding="0" cellspacing="0" width="600">
    	<tbody>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22elixirdetox%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/logo_final_tm.png%22%2C%22link%22%3A%22https%3A//www.elixirdetox.com/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.elixirdetox.com/"><img alt="elixirdetox" src="https://www.elixirdetox.com/assets/logo_final_tm.png" style="" /></a></span></td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<h2 style="font-family: 'Titillium Web', sans-serif; font-size: 33px; font-weight: 600; color: #9dcf47;">&iexcl;Bienvenido D&iacute;a 4&iexcl;</h2>
    									</td>
    								</tr>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">&iquest;C&oacute;mo te has sentido? &iquest;Te sientes mejor verdad? &iquest;C&oacute;mo va tu pelo? &iquest;Las u&ntilde;as? Lo que m&aacute;s nos gusta en Elixir es la claridad mental incomparable que llega a partir de &eacute;ste d&iacute;a &iquest;Te sientes as&iacute;?</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Si todav&iacute;a no notas cambios &iexcl;Ten paciencia, los beneficios llegar&aacute;n! Lo m&aacute;s importante es que acabes tu programa para que se logren los efectos que t&uacute; misma est&aacute;s buscando.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Algo que debes saber es que una vez que le damos la oportunidad a nuestro cuerpo de limpiarse, &eacute;ste decide aprovecharla y usar la mayor parte de su energ&iacute;a para hacerlo. As&iacute; que es muy normal que te sientas con las &ldquo;pilas un poco bajas&rdquo;. Date la oportunidad de disminuir la intensidad de tus actividades diarias y relajarte para que le permitas a tu cuerpo llevar la desintoxicaci&oacute;n de la mejor manera. Es un muy buen momento para darte un respiro.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 23px; color: #636363; font-weight: 600; line-height: 1.25em;">&iexcl;Vas por buen camino&iexcl;</p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #636363; margin: 0;"><a href="mailto:info@elixirdetox.com" style="display: block; color: #9dcf47; text-decoration: none;">info@elixirdetox.com</a></p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff; margin: 0;">01 800 00DETOX(33869)</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff;">S&iacute;guenos en:</p>

    									<p><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png%22%2C%22link%22%3A%22https%3A//www.facebook.com/ElixirDetoxMexico%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.facebook.com/ElixirDetoxMexico" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png%22%2C%22link%22%3A%22https%3A//twitter.com/elixirdetox%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://twitter.com/elixirdetox" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png%22%2C%22link%22%3A%22http%3A//instagram.com/elixirdetox/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="http://instagram.com/elixirdetox/" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png%22%2C%22link%22%3A%22http%3A//www.pinterest.com/ElixirDetoxMx/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="http://www.pinterest.com/ElixirDetoxMx/" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png%22%2C%22link%22%3A%22https%3A//www.youtube.com/user/elixirdetoxmexico%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.youtube.com/user/elixirdetoxmexico" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png" style="width: 25px; height: 28px;" width="25" /></a></span></p>
    									</td>
    									<td>
    									<p style="margin: 0; text-align: right;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22in%20juice%20we%20trust%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/slogan_footer.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="in juice we trust" src="https://www.elixirdetox.com/assets/slogan_footer.png" /></span></p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    </body>
    </html>

    '''

class Detox5Mail:
    subject='¿YA VISTE LOS CAMBIOS VERDAD?'
    body='''
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Elixir - Mail</title>
    	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600" rel="stylesheet" type="text/css" />
    </head>
    <body>

    <table align="center" bgcolor="#FFF" border="0" cellpadding="0" cellspacing="0" width="600">
    	<tbody>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540"><a href="https://www.elixirdetox.com/"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22elixirdetox%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/logo_final_tm.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="elixirdetox" src="https://www.elixirdetox.com/assets/logo_final_tm.png" style="" /></span></a></td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<h2 style="font-family: 'Titillium Web', sans-serif; font-size: 33px; font-weight: 600; color: #9dcf47;">&iexcl;Por fin&iexcl; &iexcl;D&iacute;a 5&iexcl;</h2>
    									</td>
    								</tr>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">&iexcl;Hola&iexcl; Esperamos que est&eacute;s disfrutando mucho de tu desintoxicac&iacute;n y que ya hayas comenzado a ver, sentir o experimentar los beneficios del mismo. Sin embargo es muy frecuente que cuando vamos a la mitad o estamos cerca del final los antojos nos traicionen y caigamos en la tentaci&oacute;n de salirnos de la dieta.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Si este es tu caso, antes de que caigas y detengas el proceso de desintoxicaci&oacute;n te recomiendo lo siguiente:</p>

    									<p>&nbsp;</p>

    									<ul style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">
    										<li>Toma suficiente agua durante el d&iacute;a y en especial si sientes que tienes un antojo muy fuerte de algo. Diferentes estudios han comprobado que despu&eacute;s de tomar un vaso grande con agua nuestros antojos pueden disminuir hasta en un 80% y que lo que identificamos como hambre es m&aacute;s bien sed. As&iacute; que aseg&uacute;rate de estar bien hidratado y de no caer en la tentaci&oacute;n por falta de agua.</li>
    										<li>Revisa la lista de alimentos s&oacute;lidos permitidos y ben&eacute;ficos que puedes consumir durante el Detox. Es mucho mejor que te comas una buena porci&oacute;n de apio y j&iacute;camas a que caigas en la tentaci&oacute;n de las &ldquo;papitas de la esquina&rdquo;</li>
    									</ul>

    									<p>&nbsp;</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">&iexcl;Mant&eacute;nte firme! No permitas que tu cabeza te traicione, son s&oacute;lo un par de d&iacute;as m&aacute;s para que termines, as&iacute; que aguanta ah&iacute; y no caigas en la tentaci&oacute;n de los antojos.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 23px; color: #636363; font-weight: 600; line-height: 1.25em;">&iexcl;&Aacute;nimo&iexcl;</p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #636363; margin: 0;"><a href="mailto:info@elixirdetox.com" style="display: block; color: #9dcf47; text-decoration: none;">info@elixirdetox.com</a></p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff; margin: 0;">01 800 00DETOX(33869)</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff;">S&iacute;guenos en:</p>

    									<p><a href="https://www.facebook.com/ElixirDetoxMexico" style="color: transparent; display: inline-block;" target="_blank"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="https://twitter.com/elixirdetox" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="http://instagram.com/elixirdetox/" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="http://www.pinterest.com/ElixirDetoxMx/" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png" style="width: 25px; height: 28px;" width="25" /></span> </a> <a href="https://www.youtube.com/user/elixirdetoxmexico" style="color: transparent; display: inline-block;" target="_blank"> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png" style="width: 25px; height: 28px;" width="25" /></span> </a></p>
    									</td>
    									<td>
    									<p style="margin: 0; text-align: right;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22in%20juice%20we%20trust%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/slogan_footer.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="in juice we trust" src="https://www.elixirdetox.com/assets/slogan_footer.png" /></span></p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    </body>
    </html>

    '''

class Detox6Mail:
    subject = 'POR FIN! DIA 6'
    body='''
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Elixir - Mail</title>
    	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600" rel="stylesheet" type="text/css" />
    </head>
    <body>

    <table align="center" bgcolor="#FFF" border="0" cellpadding="0" cellspacing="0" width="600">
    	<tbody>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22elixirdetox%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/logo_final_tm.png%22%2C%22link%22%3A%22https%3A//www.elixirdetox.com/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.elixirdetox.com/"><img alt="elixirdetox" src="https://www.elixirdetox.com/assets/logo_final_tm.png" style="" /></a></span></td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<h2 style="font-family: 'Titillium Web', sans-serif; font-size: 33px; font-weight: 600; color: #9dcf47;">&iexcl;Por fin&iexcl; &iexcl;D&iacute;a 6&iexcl;</h2>
    									</td>
    								</tr>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Es muy normal que para esta etapa ya te sientas revitalizado, con m&aacute;s claridad mental y m&aacute;s ligero. La dieta que has estado llevando te ha permitido alcalinizar tu cuerpo, lo cual mejora considerablemente la funci&oacute;n celular.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">El llevar una dieta con tendencia alcalina, como la que proveen nuestros programas, le permite a tus c&eacute;lulas desempe&ntilde;ar sus funciones de la manera en que lo deber&iacute;an de hacer siempre. Y como somos parte de un todo, si a nivel celular tu cuerpo trabaja bien, quiero que te imagines c&oacute;mo lo hace a nivel macro.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 13px; color: #636363; line-height: 1.25em;">Te felicitamos por continuar hasta aqu&iacute; y por permitirte darle este respiro de vitalidad a tu cuerpo, desde cada una de tus c&eacute;lulas.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 23px; color: #636363; font-weight: 600; line-height: 1.25em;">&iexcl;&Aacute;nimo&iexcl; &iexcl;Solo te queda un d&iacute;a m&aacute;s!.</p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #636363; margin: 0;"><a href="mailto:info@elixirdetox.com" style="display: block; color: #9dcf47; text-decoration: none;">info@elixirdetox.com</a></p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff; margin: 0;">01 800 00DETOX(33869)</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff;">S&iacute;guenos en:</p>

    									<p><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png%22%2C%22link%22%3A%22https%3A//www.facebook.com/ElixirDetoxMexico%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.facebook.com/ElixirDetoxMexico" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png%22%2C%22link%22%3A%22https%3A//twitter.com/elixirdetox%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://twitter.com/elixirdetox" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png%22%2C%22link%22%3A%22http%3A//instagram.com/elixirdetox/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="http://instagram.com/elixirdetox/" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png%22%2C%22link%22%3A%22http%3A//www.pinterest.com/ElixirDetoxMx/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="http://www.pinterest.com/ElixirDetoxMx/" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png%22%2C%22link%22%3A%22https%3A//www.youtube.com/user/elixirdetoxmexico%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.youtube.com/user/elixirdetoxmexico" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png" style="width: 25px; height: 28px;" width="25" /></a></span></p>
    									</td>
    									<td>
    									<p style="margin: 0; text-align: right;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22in%20juice%20we%20trust%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/slogan_footer.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="in juice we trust" src="https://www.elixirdetox.com/assets/slogan_footer.png" /></span></p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    </body>
    </html>

    '''

class BirthdayMail:
    def __init__(self,code,name):
        subject='¡Sabemos que es tu cumple y queremos festejarte!'
        body='''
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        	<title>Elixir - Mail</title>
        	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1">
        	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600" rel="stylesheet" type="text/css" />
        </head>
        <body>

        <table align="center" bgcolor="#FFF" border="0" cellpadding="0" cellspacing="0" width="600">
        	<tbody>
        		<tr>
        			<td>
        			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
        				<tbody>
        					<tr>
        						<td height="30" width="30">&nbsp;</td>
        						<td height="30" width="540">&nbsp;</td>
        						<td height="30" width="30">&nbsp;</td>
        					</tr>
        					<tr>
        						<td width="30">&nbsp;</td>
        						<td width="540"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22elixirdetox%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/logo_final_tm.png%22%2C%22link%22%3A%22https%3A//www.elixirdetox.com/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.elixirdetox.com/"><img alt="elixirdetox" src="https://www.elixirdetox.com/assets/logo_final_tm.png" style="" /></a></span></td>
        						<td width="30">&nbsp;</td>
        					</tr>
        					<tr>
        						<td height="30" width="30">&nbsp;</td>
        						<td height="30" width="540">&nbsp;</td>
        						<td height="30" width="30">&nbsp;</td>
        					</tr>
        				</tbody>
        			</table>
        			</td>
        		</tr>
        		<tr>
        			<td>
        			<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="600">
        				<tbody>
        					<tr>
        						<td height="30" width="30">&nbsp;</td>
        						<td height="30" width="540">&nbsp;</td>
        						<td height="30" width="30">&nbsp;</td>
        					</tr>
        					<tr>
        						<td width="30">&nbsp;</td>
        						<td width="540">
        						<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="540">
        							<tbody>
        								<tr>
        									<td>
        									<h2 style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 25px; font-weight: 600; color: #9dcf47;">&iexcl;Hola! '''+name+'''</h2>
        									</td>
        								</tr>
        								<tr>
        									<td>
        									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 33px; color: #636363; margin: 0; line-height: 1.25em;">&iexcl;Felicidades!</p>

        									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 23px; font-weight: 600; color: #636363; margin: 0; line-height: 1.25em;">Sabemos que es tu cumplea&ntilde;os y queremos ayudarte festejar sin remordimientos.</p>

        									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 23px; color: #636363; margin: 0; line-height: 1.25em;">Resetea tu cuerpo con un detox y nosotros te regalamos un</p>

        									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 33px; color: #636363; margin: 0; line-height: 1.25em;"><span style="color: #9dcf47; font-weight: 600;">25% de descuento</span></p>

        									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 23px; color: #636363; line-height: 1.25em;">Tu c&oacute;digo de descuento es:</p>

        									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 23px; font-weight: 600; color: #9dcf47; line-height: 1.25em;">'''+code+'''</p>

        									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 23px; font-weight: 600;"><a href="http://www.elixirdetox.com/products" style="background: #9dcf47 !important; color: #FFF !important; font-size: 21px; padding: 10px 30px !important; display: inline-block; text-decoration: none; text-align: center; margin: 0 auto; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;" target="_blank">Ordena Aqu&iacute;</a></p>

        									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 12px; color: #636363; line-height: 1.25em;">Vigencia de dos meses a partir de tu cumplea&ntilde;os</p>
        									</td>
        								</tr>
        							</tbody>
        						</table>
        						</td>
        						<td width="30">&nbsp;</td>
        					</tr>
        				</tbody>
        			</table>
        			</td>
        		</tr>
        		<tr>
        			<td><span class="sg-image" data-imagelibrary="%7B%22width%22%3A600%2C%22height%22%3A229%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//s3.amazonaws.com/elixirdetox/app/public/spree/images_email/9/original/felicidades_elixir_detox.png%3F2015%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img height="229" src="https://s3.amazonaws.com/elixirdetox/app/public/spree/images_email/9/original/felicidades_elixir_detox.png?2015" style="border: 0px; width: 600px; height: 229px;" width="600" /></span></td>
        		</tr>
        		<tr>
        			<td>
        			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
        				<tbody>
        					<tr>
        						<td height="30" width="30">&nbsp;</td>
        						<td height="30" width="540">&nbsp;</td>
        						<td height="30" width="30">&nbsp;</td>
        					</tr>
        					<tr>
        						<td width="30">&nbsp;</td>
        						<td width="540">
        						<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="540">
        							<tbody>
        								<tr>
        									<td>
        									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #636363; margin: 0;"><a href="mailto:info@elixirdetox.com" style="display: block; color: #9dcf47; text-decoration: none;">info@elixirdetox.com</a></p>

        									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff; margin: 0;">01 800 00DETOX(33869)</p>

        									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff;">S&iacute;guenos en:</p>

        									<p><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png%22%2C%22link%22%3A%22https%3A//www.facebook.com/ElixirDetoxMexico%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.facebook.com/ElixirDetoxMexico" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png%22%2C%22link%22%3A%22https%3A//twitter.com/elixirdetox%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://twitter.com/elixirdetox" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png%22%2C%22link%22%3A%22http%3A//instagram.com/elixirdetox/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="http://instagram.com/elixirdetox/" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png%22%2C%22link%22%3A%22http%3A//www.pinterest.com/ElixirDetoxMx/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="http://www.pinterest.com/ElixirDetoxMx/" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png%22%2C%22link%22%3A%22https%3A//www.youtube.com/user/elixirdetoxmexico%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.youtube.com/user/elixirdetoxmexico" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png" style="width: 25px; height: 28px;" width="25" /></a></span></p>
        									</td>
        									<td>
        									<p style="margin: 0; text-align: right;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22in%20juice%20we%20trust%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/slogan_footer.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="in juice we trust" src="https://www.elixirdetox.com/assets/slogan_footer.png" /></span></p>
        									</td>
        								</tr>
        							</tbody>
        						</table>
        						</td>
        						<td width="30">&nbsp;</td>
        					</tr>
        					<tr>
        						<td height="30" width="30">&nbsp;</td>
        						<td height="30" width="540">&nbsp;</td>
        						<td height="30" width="30">&nbsp;</td>
        					</tr>
        				</tbody>
        			</table>
        			</td>
        		</tr>
        	</tbody>
        </table>
        </body>
        </html>

        '''

class SuperfoodRecipesMail:
    subject='Recetas para tus Superfoods'
    body='''
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<title>Elixir - Mail</title>
    	<meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600" rel="stylesheet" type="text/css" />
    </head>
    <body>
    <p style="text-align: center;">&lt;%body%&gt;</p>

    <table align="center" bgcolor="#FFF" border="0" cellpadding="0" cellspacing="0" width="600">
    	<tbody>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A206%2C%22height%22%3A44%2C%22alt_text%22%3A%22elixirdetox%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/logo_final_tm.png%22%2C%22link%22%3A%22https%3A//www.elixirdetox.com/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.elixirdetox.com/"><img alt="elixirdetox" height="44" src="https://www.elixirdetox.com/assets/logo_final_tm.png" style="width: 206px; height: 44px;" width="206" /></a></span></td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<h2 style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 25px; font-weight: 600; color: #9dcf47;">&iexcl;Gracias por tu compra de superfoods!</h2>
    									</td>
    								</tr>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 14px; color: #636363; margin: 0 0 1em; line-height: 1.25em;">Puede que seas nuevo en el <strong>&ldquo;healthy lifestyle&rdquo;</strong> o seas todo un experto en saber como usar tus nuevos productos, en Elixir, lo que queremos es hacerte la vida m&aacute;s f&aacute;cil.</p>

    									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 14px; color: #636363; margin: 0; line-height: 1.25em;">Te mandamos unas recetas que te pueden ayudar a hacer peque&ntilde;os cambios en la alimentaci&oacute;n. Todas est&aacute;n deliciosas.</p>

    									<p><span class="sg-image" data-imagelibrary="%7B%22width%22%3A540%2C%22height%22%3A536%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/store/recetas_4.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img height="536" src="https://www.elixirdetox.com/assets/store/recetas_4.png" style="border: 0px; width: 540px; height: 536px;" width="540" /></span></p>

    									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 14px; color: #636363; margin: 0; line-height: 1.25em;">&iquest;Quieres ver m&aacute;s recetas deliciosas?</p>

    									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 14px; font-weight: 600;"><a href="https://www.elixirdetox.com/receips" style="background: #9dcf47 !important; color: #FFF !important; font-size: 21px; padding: 10px 30px !important; display: inline-block; text-decoration: none; text-align: center; margin: 0 auto; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;" target="_blank">Descarga muchas m&aacute;s AQU&Iacute;</a></p>

    									<p style="font-family: 'Titillium Web', sans-serif; text-align: center; font-size: 14px; color: #636363; margin: 0 0 2em; line-height: 1.25em;">O s&iacute;guenos en Instagram <a href="https://www.instagram.com/elixirdetox/" style="color: #636363;">@elixirdetox</a><br />
    									Todas las semanas posteamos nuevas.</p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    		<tr>
    			<td>
    			<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="600">
    				<tbody>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td width="30">&nbsp;</td>
    						<td width="540">
    						<table align="center" bgcolor="#2f2f2f" border="0" cellpadding="0" cellspacing="0" width="540">
    							<tbody>
    								<tr>
    									<td>
    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #636363; margin: 0;"><a href="mailto:info@elixirdetox.com" style="display: block; color: #9dcf47; text-decoration: none;">info@elixirdetox.com</a></p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff; margin: 0;">01 800 00DETOX(33869)</p>

    									<p style="font-family: 'Titillium Web', sans-serif; font-size: 14px; font-weight: 600; color: #ffffff;">S&iacute;guenos en:</p>

    									<p><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png%22%2C%22link%22%3A%22https%3A//www.facebook.com/ElixirDetoxMexico%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.facebook.com/ElixirDetoxMexico" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/11c1e6eb609614b01d4fafd5490a395319044fcc383ad4c96d61e1ce__30daccc7-1c63-41c8-9c3d-d182b0e3980e.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png%22%2C%22link%22%3A%22https%3A//twitter.com/elixirdetox%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://twitter.com/elixirdetox" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/26a8956f9a8bedd8f148ddde88f2a9ce963d3f2a64ead93bdeb6fc04__5f3e8d87-265c-4ac9-a2d1-6acdab2910d0.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png%22%2C%22link%22%3A%22http%3A//instagram.com/elixirdetox/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="http://instagram.com/elixirdetox/" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/5437f518c3359272cfad8468324e6ea7cd02b6b4321d8cc810137574__8ec9bd5f-8210-4cdd-adef-55b31a2bf4f6.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png%22%2C%22link%22%3A%22http%3A//www.pinterest.com/ElixirDetoxMx/%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="http://www.pinterest.com/ElixirDetoxMx/" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/c61b5a3b839f254db573a5ab8c984a708a7f09d79e685a3b762543ac__223ba8cf-ceed-4753-968d-0b11a057faeb.png" style="width: 25px; height: 28px;" width="25" /></a></span> <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2225%22%2C%22height%22%3A%2228%22%2C%22alt_text%22%3A%22%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png%22%2C%22link%22%3A%22https%3A//www.youtube.com/user/elixirdetoxmexico%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a href="https://www.youtube.com/user/elixirdetoxmexico" style="color: transparent; display: inline-block;" target="_blank"><img alt="" border="0" height="28" src="https://marketing-image-production.s3.amazonaws.com/uploads/7e8c72c77ab24a8ec26a86b069c6e85e65c511d795faea101ce5d8af335c7c043e51c4a93bac1d3ba5097db48643820a5439a49cb619c4639947c213d960e708/4fb580d0cc045e981ca7f1c98d0936247a258a1ccec271bb149a54e5__885aab91-f29a-4243-8b8f-9a917268852e.png" style="width: 25px; height: 28px;" width="25" /></a></span></p>
    									</td>
    									<td>
    									<p style="margin: 0; text-align: right;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A0%2C%22height%22%3A0%2C%22alt_text%22%3A%22in%20juice%20we%20trust%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//www.elixirdetox.com/assets/slogan_footer.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><img alt="in juice we trust" src="https://www.elixirdetox.com/assets/slogan_footer.png" /></span></p>
    									</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    						<td width="30">&nbsp;</td>
    					</tr>
    					<tr>
    						<td height="30" width="30">&nbsp;</td>
    						<td height="30" width="540">&nbsp;</td>
    						<td height="30" width="30">&nbsp;</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    	</tbody>
    </table>
    </body>
    </html>

    '''
