from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
from base import mailService
from middleware import interceptor
from system import exceptionCtrl
from config import dev_env
import requests
import datetime as dt


def quotation(cart_id):
    cart = db.cart.find_one({'_id':ObjectId(cart_id)})
    prev_q=[]
    for pq in db.quotes.find({'cart_id':str(cart['_id'])}):
        prev_q.append(pq)
    if len(prev_q)>0:
        #Delete old quotes
        for q in prev_q:
            db.quotes.remove({'_id':q['_id']})
        # Chack if cart has previous quote assigned
        if 'quote_id' in cart:
            cart.pop('quote_id',None)
        if cart['shipping_cost']>0:
            cart['amount']-=cart['shipping_cost']
            cart['shipping_cost']=0
        db.cart.update_one({'_id':ObjectId(cart_id)},{"$set":cart})
    free = False
    for item in cart['cart']:
        if item['variantCode']=='6SUS' or item['variantCode']=='12SUS':
            q={'total':99}
            db.quotes.insert_one(q)
            return [{'_id':str(q['_id']),'total':99}]
    for item in cart['cart']:
        if item['variantCode']=='6COH':
            free=True
        if item['variantCode']=='12COH':
            free=True
    if cart['address']['city']=='CDMX':
        #Use shippify
        deliveries = []
        pickup = {
          "contact": {
            "name": "Usuario Pruebas",
            "email": "jcamarena@camlab.mx",
            "phonenumber": "5510825870"
          },
          "location": {
            "address": "Extremadura 153, Insurgentes Mixcoac, Benito Juárez, 03290, CDMX",
            "instructions": "Interior 4"
          }
        }
        dropoff = {}
        dropoff['contact']={}
        dropoff['location']={}
        dropoff['contact']['name']=cart['user']['name']+" "+cart['user']['last_name']
        dropoff['contact']['email']=cart['user']['email']
        dropoff['contact']['phonenumber']=cart['address']['phone']
        dropoff['location']['address']=cart['address']['street']+","+cart['address']['street2']+","+cart['address']['street3']+","+cart['address']['zip']
        dropoff['location']['instructions']=cart['address']['reference']
        dropoff['location']['lat']=cart['address']['coords']['lat']
        dropoff['location']['lng']=cart['address']['coords']['lng']
        packages=[]
        for item in cart['cart']:
            packages.append({"id":item['product'],"name":"Colágeno Hidrolizado","size":"xs","qty":item['quantity']})
        deliveries.append({"pickup":pickup,"dropoff":dropoff,"packages":packages,"referenceId":str(cart['_id'])})
        headers = {"Authorization":"Basic ajh6aDI1OHlvdG9rMjFhbjk2YjViNm5ydHZzaGJiajRpOmo4emgyNXp4OXZmZm1lODdmcGc0NHBsZGk=","Content-type":"application/json"}
        res = requests.post("https://api.shippify.co/v1/deliveries/quotes",headers=headers, json = {"deliveries":deliveries})
        res = res.json()
        for q in res['payload']['quotes']:
            q['carrier']='shippify'
            q['cart_id']=str(cart['_id'])
            db.quotes.insert_one(q)
        return []
    else:
        #EnvioClick
        api_url = 'https://api.envioclickpro.com/api/v1'
        headers = {"Authorization":"749a392f-c418-4202-83a4-bc57357f89c3","Content-type":"application/json"}

        body = {}
        body['package']={
            'description':'Colágeno Hidrolizado',
            'contentValue':cart['subtotal'],
            'weight':1,
            'length':12,
            'height':21,
            'width':11
        }
        body['origin_zip_code']='03920'
        body['destination_zip_code']=str(cart['address']['zip'])
        res = requests.post(api_url+'/quotation',headers=headers, json = body)
        rates = res.json()['data']['rates']
        cost_rates = []
        cost_dict = {}
        fastest_rate = []
        prod_dict = {}
        res = {"fastest":None,"cheaper":None}
        for item in rates:
            cost_rates.append(item['total'])
            cost_dict[item['total']]=item
            fastest_rate.append(item['idProduct'])
            prod_dict[item['idProduct']]=item
        faster_products = [16,14,12,11]
        for i in faster_products:
            if not res['fastest']:
                if i in fastest_rate:
                    res['fastest']=prod_dict[i]
                    res['fastest']['cart_id']=str(cart['_id'])
        cost_rates.sort()
        res['cheaper']=cost_dict[cost_rates[0]]
        res['cheaper']['cart_id']=str(cart['_id'])
        db.quotes.insert_one(res['cheaper'])
        db.quotes.insert_one(res['fastest'])
        res['cheaper']['_id']=str(res['cheaper']['_id'])
        res['fastest']['_id']=str(res['fastest']['_id'])
        arr = [res['cheaper'],res['fastest']]
        if free:
            return [{'total':0}]
        else:
            return arr

def setQuote(cart_id,quote_id,client_pays):
    try:
        cart = db.cart.find_one({'_id':ObjectId(cart_id)})
        quote = db.quotes.find_one({'_id':ObjectId(quote_id)})
        # Add shipping cost
        cost = None
        if 'total' in quote:
            cost = quote['total']
        if 'cost' in quote:
            cost = quote['cost']
        if client_pays:
            cart['shipping_cost']=cost
            cart['amount']+=cost
        cart['quote_id']=quote_id
        db.cart.update_one({'_id':cart['_id']},{"$set":cart})
        cart['_id']=str(cart['_id'])
        return cart
    except Exception as e:
        return False

class QuotationAPI(MethodView):
    def get(self,cart_id):
        try:
            data = db.quotes.find({'cart':cart_id})
            res = []
            for quote in data:
                res.append({'_id':str(quote['_id']),'cost':quote['client_cost'],'details':quote['details']})
            return jsonify({'quotes':res})
        except Exception as e:
            exceptionCtrl.StoreException('ecommerce/shipping.QuotationAPI.get',{},str(e))
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response

    def post(self):
        data = request.get_json(force=True)
        try:
            #Get carts object
            # cart = db.cart.find_one({'_id':ObjectId(data['cart_id'])})
            quotes = quotation(data['cart_id'])
            return jsonify({'quotes':quotes})
        except Exception as e:
            exceptionCtrl.StoreException('ecommerce/shipping.QuotationAPI.post',{},str(e))
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response

class SetQuoteAPI(MethodView):
    def post(self):
        data = request.get_json(force=True)
        try:
            #Get carts object
            # cart = db.cart.find_one({'_id':ObjectId(data['cart_id'])})
            cart = setQuote(data['cart_id'],data['quote_id'],True)
            if cart:
                return jsonify({'cart':cart})
            else:
                response = jsonify({'error':'Error, revisa tus datos por favor'})
                response.status_code=400
                return response
        except Exception as e:
            exceptionCtrl.StoreException('ecommerce/shipping.SetQuotationAPI.post',{},str(e))
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response
