from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db

class CityAPI(MethodView):
    def get(self):
        try:
            #Retrieve only show on web cities
            cities=db.cities.find({'show_in_web':1},{'name':1,'category':1})
            res=[]
            for city in cities:
                res.append({'name':city['name'],'category':city['category']})
            return jsonify({"cities":res})
        except Exception as e:
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response
