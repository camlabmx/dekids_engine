from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
from base import mailService
from middleware import interceptor
from system import exceptionCtrl
from config import dev_env
import requests
import datetime as dt
from ecommerce import mail_templates
import hashlib
import openpay

def pagoFacilCardPayment(cart,payment):
    service_id='3'
    if dev_env:
        pagofacil_url = 'https://stapi.pagofacil.net/Wsrtransaccion/index/format/json?method=transaccion&'
        id_sucursal = 'fa8b0717ada677211c1d1ee8ebb91b15871c7d2a'
        id_usuario = '7c29666005e3c6645a4bc268cb77bf07f7471a78'
    else:
        pagofacil_url = 'https://www.pagofacil.net/ws/public/Wsrtransaccion/index/format/json?method=transaccion'
        id_sucursal = 'e9481e61aa28d34a66ababa3d184449aab56b907'
        id_usuario = '0c5cc1c79fdc7b873fa30f8b009a4de96875bcfb'
    str_data = ''
    str_data+='data[idServicio]='+str(service_id)+'&'
    str_data+='data[idSucursal]='+str(id_sucursal)+'&'
    str_data+='data[idUsuario]='+str(id_usuario)+'&'
    str_data+='data[nombre]='+str(payment['card']['name'])+'&'
    str_data+='data[apellidos]='+str(payment['card']['last_name'])+'&'
    str_data+='data[numeroTarjeta]='+str(payment['card']['number'])+'&'
    str_data+='data[cvt]='+str(payment['card']['cvv'])+'&'
    str_data+='data[cp]='+str(payment['bill_address']['zip'])+'&'
    str_data+='data[mesExpiracion]='+str(payment['card']['validTo'][:2])+'&'
    str_data+='data[anyoExpiracion]='+str(payment['card']['validTo'][3:])+'&'
    str_data+='data[monto]='+str(cart['amount'])+'&'
    str_data+='data[email]='+str(cart['user']['email'])+'&'
    str_data+='data[telefono]='+str(payment['bill_address']['phone'])+'&'
    str_data+='data[celular]='+str(payment['bill_address']['cell'])+'&'
    str_data+='data[calleyNumero]='+str(payment['bill_address']['street'])+'&'
    str_data+='data[colonia]='+str(payment['bill_address']['street2'])+'&'
    str_data+='data[municipio]='+str(payment['bill_address']['town'])+'&'
    str_data+='data[estado]='+str(payment['bill_address']['state'])+'&'
    str_data+='data[pais]='+str(payment['bill_address']['country'])+'&'
    str_data+='data[idPedido]='+str(cart['_id'])

    result = requests.post(pagofacil_url+str_data)
    data = result.json()
    if 'autorizado' in data['WebServices_Transacciones']['transaccion'] and data['WebServices_Transacciones']['transaccion']['autorizado']=='1':
        transaction = data['WebServices_Transacciones']['transaccion']
        db.cart.update({'_id':cart['_id']},{'$set':
            {
                'status':'checkout',
                'checkout_datetime':dt.datetime.now().strftime('%d/%m/%Y %H:%M'),
                'payment_status':'ok',
                'payment_type':'card',
                'payment':
                    {
                        'type':'card',
                        'card_type':transaction['TipoTC'],
                        'authorization':transaction['autorizacion'],
                        'transaction':transaction['transaccion'],
                        'datetime':transaction['TransFin'],
                        'bill_address':payment['bill_address'],
                        'card':transaction['dataVal']['numeroTarjeta']
                    }
            }
        })
        status = data['WebServices_Transacciones']['transaccion']['autorizado']
    error=None
    if 'error' in data['WebServices_Transacciones']['transaccion']:
        error = data['WebServices_Transacciones']['transaccion']['error']
    if error and data['WebServices_Transacciones']['transaccion']['autorizado']=='0':
        if dev_env:
            print(error)
        return transactionError(error)
    else:
        return True

def processCardPayment(cart,deviceSessionId,token):
    try:
        openpay.api_key = "sk_c1530cb3dd0b402db6d6b5f2d3733bf2"
        openpay.verify_ssl_certs = False
        openpay.merchant_id = "mcrlq5w1sokxqmrlpsqg"
        openpay.production = False
        customer = openpay.Customer.create(
            name=cart['user']['name'],
            email=cart['user']['email'],
            address={
                "city": cart['address']['city'],
                "state":cart['address']['state'],
                "line1":cart['address']['street'],
                "postal_code":cart['address']['zip'],
                "line2":cart['address']['street2'],
                "line3":cart['address']['reference'],
                "country_code":"MX"
            },
            last_name=cart['user']['last_name'],
            phone_number=cart['address']['phone']
        )
        #Load desc str
        desc=''
        for item in cart['cart']:
            desc+=str(item['quantity'])+':'+item['variantCode']+','
        charge = customer.charges.create(source_id=token, method="card", amount= round(cart['amount'],2), description=desc+" Colágeno ARCANA",order_id=str(cart['_id']),device_session_id=str(deviceSessionId))
        return True,charge
    except Exception as e:
        print(str(e))
        exceptionCtrl.StoreException('checkout.processCardPayment',{},str(e))
        return False,"Error"

def transactionError(error):
    error_str = ''
    if type(error) is dict:
        for key in error.keys():
            error_str +='Intenta nuevamente: '+error[key]+'<br/>'
    else:
        error_str=error
    return error_str
def setTrackID(cart_id):
    trackId='A'+str(int(hashlib.sha1(cart_id.encode('utf8')).hexdigest(), 16) % (10 ** 8))
    db.cart.update({'_id':ObjectId(cart_id)},{'$set':{'track_id':trackId}})
def sendMail(cart):
    try:
        mailService.sendMail(cart['user']['email'],'Compra Exitosa',mail_templates.successfulPurchaseMail(cart))
    except Exception as e:
        print("sendMail",e)
        exceptionCtrl.StoreException('checkout.sendMail',cart,str(e))
        response = jsonify({'error':'Error al enviar el correo de confirmación'})
        return response

def createShippingRecords(cart_id,cart):
    detox = []
    products = []
    for item in cart['cart']:
        prod = db.products.find_one({'short_name':item['productName']})
        for variant in prod['variants']:
            if variant['code'] == item["variantCode"]:
                if 'days' in variant:
                    detox.append({'item':item,'variant':variant})
                else:
                    products.append(item)
    #Create shipping records
    if len(detox)>0:
        for item in detox:
            day = 1
            for i in range(0,item['variant']['days']):
                ship = {'order':cart_id}
                ship['delivery']=dt.datetime.strftime(dt.datetime.now()+dt.timedelta(days=day),'%d/%m/%Y')
                day+=30

                ship['ship'] = [item['item']]
                ship['status']='pending'
                ship['address']=cart['address']
                ship['user']=cart['user']
                db.shipping.insert(ship)
    else:
        #If there's not detox
        ship = {'order':cart_id}
        ship['delivery']=dt.datetime.strftime(dt.datetime.now()+dt.timedelta(days=1),'%d/%m/%Y')
        ship['ship']=products
        ship['status']='pending'
        ship['address']=cart['address']
        ship['user']=cart['user']
        db.shipping.insert(ship)

def blockInventory(cart):
    try:
        for item in cart:
            # Get product inventory_reqs
            product = db.products.find_one({'_id':ObjectId(item['product'])},{'inventory_reqs':1})
            if product:
                if 'inventory_reqs' in product:
                    inventory_reqs = product['inventory_reqs']
                    for req in inventory_reqs:
                        # Get req inventory object
                        inventory = db.inventory.find_one({'_id':ObjectId(req['_id'])})
                        # block inventory
                        inventory['blocked']+=req['quantity']*item['quantity']
                        inventory['inventory']-=req['quantity']*item['quantity']
                        db.inventory.update({'_id':inventory['_id']},{'$set':inventory})
    except Exception as e:
        exceptionCtrl.StoreException('checkout.blockInventory',cart,str(e))
        response = jsonify({'error':'Error al procesar la compra, inténtalo de nuevo'})
        response.statusText='Error al procesar la compra, inténtalo de nuevo'
        response.status_code = 500
        return response
def processOrder(cart_id):
    try:
        setTrackID(cart_id)
        cart = db.cart.find_one({'_id':ObjectId(cart_id)})
        createShippingRecords(cart_id,cart)
        sendMail(cart)
        # blockInventory(cart['cart'])
        return True
    except Exception as e:
        exceptionCtrl.StoreException('checkout.processOrder',cart,str(e))
        response = jsonify({'error':'Error al procesar la compra, inténtalo de nuevo'})
        response.statusText='Error al procesar la compra, inténtalo de nuevo'
        response.status_code = 500
        return response
class CheckoutAPI(MethodView):

    def post(self):
        data = None
        try:
            # permission, user = interceptor.headerInterceptor(request.headers,[])
            data = request.get_json(force=True)
            cart = db.cart.find_one({'_id':ObjectId(data['_id'])})
            #Check carts shipping address
            if not 'address' in cart:
                #Set user address
                db.cart.update({'_id':cart['_id']},{'$set':{'address':cart['user']['address'][0]}})
            #First check payment type
            if data['type']=='card':
            # Pago Facil
            # if data['payment']['type']=='card':
                # payment_response = processCardPayment(cart,data['payment'])
                # Open Pay
                payment_response,payment_data = processCardPayment(cart,data['deviceSessionId'],data['token'])
                if payment_response == True:
                    db.cart.update({'_id':ObjectId(data['_id'])},{'$set':{
                        'status':'checkout',
                        'checkout_datetime':dt.datetime.now().strftime('%d/%m/%Y %H:%M'),
                        'payment_status':'ok',
                        'payment_type':'card',
                        'payment_data':payment_data
                    }})
                    processOrder(data['_id'])
                else:
                    response = jsonify({"error":payment_response})
                    response.statusText=payment_response
                    response.status_code = 400
                    return response
            elif data['payment']['type']=='cash':
                db.cart.update({'_id':ObjectId(data['_id'])},{'$set':{
                    'status':'checkout',
                    'checkout_datetime':dt.datetime.now().strftime('%d/%m/%Y %H:%M'),
                    'payment_status':'pending',
                    'payment_type':'cash'
                }})
                processOrder(data['_id'])
            elif data['payment']['type']=='paypal':
                db.cart.update({'_id':ObjectId(data['_id'])},{'$set':{
                    'status':'checkout',
                    'checkout_datetime':dt.datetime.now().strftime('%d/%m/%Y %H:%M'),
                    'payment_status':'ok',
                    'payment_type':'paypal',
                    'paypal_data':data['paypal_data']
                }})
                processOrder(data['_id'])
            elif data['payment']['type']=='costco':
                db.cart.update({'_id':ObjectId(data['_id'])},{'$set':{
                    'status':'checkout',
                    'checkout_datetime':dt.datetime.now().strftime('%d/%m/%Y %H:%M'),
                    'payment_status':'ok',
                    'payment_type':'costco'
                }})
                processOrder(data['_id'])
                #redeeme code
                pass
            else:
                exceptionCtrl.StoreException('checkout.CheckoutAPI.post',data,'Información de pago incompleta')
                response = jsonify({})
                response.statusText='Información de pago incompleta'
                response.status_code = 400
                return response
            return jsonify({})
        except Exception as e:
            print(str(e))
            exceptionCtrl.StoreException('checkout.CheckoutAPI.post',data,str(e))
            response = jsonify({'error':str(e)})
            response.statusText='Error al procesar la compra, inténtalo de nuevo'
            response.status_code = 500
            return response
class PaypalCheckoutAPI(MethodView):
    def post(self,cart_id,p_type,payment_id,payer_id,status):
        data = None
        try:
            cart = db.cart.find_one({'_id':ObjectId(cart_id)})
            if not 'address' in cart:
                #Set user address
                db.cart.update({'_id':cart['_id']},{'$set':{'address':cart['user']['address'][0]}})
            if p_type=='paypal':
                data = {
                    'status':'checkout',
                    'checkout_datetime':dt.datetime.now().strftime('%d/%m/%Y %H:%M'),
                    'payment_status':'ok',
                    'payment_type':'paypal',
                    'paypal_data':{"id":payment_id,"payer_id":payer_id,"status":status}
                }
                db.cart.update({'_id':ObjectId(cart_id)},{'$set':data})
                processOrder(cart_id)
            return jsonify({})
        except Exception as e:
            print(str(e))
            exceptionCtrl.StoreException('checkout.PaypalCheckoutAPI.post',data,str(e))
            response = jsonify({'error':str(e)})
            response.statusText='Error al procesar la compra, inténtalo de nuevo'
            response.status_code = 500
            return response
