from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
from system import exceptionCtrl

class ContentAPI(MethodView):
    def get(self,section,_id=None):
        try:
            if section=='blog':
                res = db.content.find({'section':section}).sort('index',-1)
                if _id:
                    res = db.content.find_one({'section':section,'index':_id})
                    res['_id']=str(res['_id'])
                    return jsonify({'data':res})
            elif section=='press':
                res = db.content.find({'section':section}).sort('order',-1)
            elif section=='faq':
                res = db.content.find({'section':section}).sort('order',1)
            else:
                res = db.content.find({'section':section})
            data = []
            for reg in res:
                reg['_id']=str(reg['_id'])
                data.append(reg)
            return jsonify({'data':data})
        except Exception as e:
            exceptionCtrl.StoreException('page_content.ContentAPI.get',{'section':section},str(e))
            print(e)
            res = jsonify({})
            res.status_text=500
            return res
