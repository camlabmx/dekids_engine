from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db


class MainSliderAPI(MethodView):
    def get(self,section):
        cursor = db.slides.find({"type":section})
        result = []
        for p in cursor:
            p['_id']=str(p['_id'])
            result.append(p)
        return jsonify({"data":result})#JSONEncoder().encode(result)
