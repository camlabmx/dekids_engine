from flask.views import MethodView
from flask import jsonify,request
from middleware import interceptor
from bson import ObjectId
from database import db
from system import exceptionCtrl
import datetime as dt

class ShoppingCart():
    def startCartSession(self,user,shippingCity):
        try:
            #If not we're going to create carts object
            cart_session = {}
            if shippingCity:
                cart_session['shippingCity']=shippingCity
            if user and user['role']=='client':
                cart_session['user']=user
            cart_session['cart']=[]
            cart_session['shipping_cost']=0
            cart_session['amount']=0
            cart_session['startTime']=dt.datetime.now().strftime('%d/%m/%Y %H:%m')
            cart_session['status']='InProgress'
            cart_session['detox']=False
            #Store cart new object
            cart = db.cart.insert_one(cart_session)
            return True,str(cart.inserted_id)
        except Exception as e:
            exceptionCtrl.StoreException('ShoppingCart.startCartSession',{'user':user,'shippingCity':shippingCity},str(e))
            response = jsonify({"error":"ERROR"})
            response.status_code = 500
            return False,response
    def cartOperation(self,cart_id,operation,operator):
        try:
            # Get stored cart
            stored_cart = db.cart.find_one({'_id':ObjectId(cart_id)})
            stored_cart['_id']=str(stored_cart['_id'])
            # Look for existing product
            remove = None
            applied = False
            amount = 0.0
            subtotal = 0.0
            if 'amount' in stored_cart:
                amount = float(stored_cart['amount'])
            if 'subtotal' in stored_cart:
                subtotal = float(stored_cart['subtotal'])
            for item in stored_cart['cart']:
                if item['product']==operation['product']:
                    if item['variantCode']==operation['variantCode']:
                        if operator=='+':
                            item['quantity']+=1
                            item['amount']+=float(item['price'])
                            item['subtotal']+=float(item['price'])
                            applied=True
                            amount+=float(item['price'])
                            subtotal+=float(item['price'])
                        else:
                            item['quantity']-=1
                            item['amount']-=float(item['price'])
                            item['subtotal']-=float(item['price'])
                            applied=True
                            amount-=float(item['price'])
                            subtotal-=float(item['price'])
                            if item['quantity']==0:
                                remove=item
            #Remove 0 quantity products
            if remove:
                with_detox = False
                if 'discount' in remove:
                    amount+=remove['discount']['amount']
                    db.cart.update({'_id':ObjectId(cart_id)},{'$set':{'applied_code':None}})
                    stored_cart['applied_code']=None
                stored_cart['cart'].remove(remove)
                #Check if it still have detox
                for item in stored_cart['cart']:
                    if 'start_detox' in item and item['start_detox']:
                        with_detox=True
                stored_cart['detox']=with_detox
            if not applied:
                amount+=float(operation['amount'])
                subtotal+=float(operation['amount'])
                operation['amount'] = float(operation['amount'])
                operation['subtotal'] = float(operation['amount'])
                operation['price'] = float(operation['amount'])
                stored_cart['cart'].append(operation)
                if 'start_detox' in operation and operation['start_detox']:
                    amount-=stored_cart['shipping_cost']
                    stored_cart['shipping_cost']=0
                    stored_cart['detox']=True
            if amount<0:
                amount=0
            if subtotal<0:
                subtotal=0
            stored_cart['amount']=amount
            stored_cart['subtotal']=subtotal
            # Update cart
            db.cart.update({'_id':ObjectId(cart_id)},{'$set':{'cart':stored_cart['cart'],'subtotal':subtotal,'amount':amount,'shipping_cost':stored_cart['shipping_cost'],'detox':stored_cart['detox']}})
            stored_cart['_id']=str(stored_cart['_id'])
            response = jsonify({'cart':stored_cart})
            response.status_code=200
        except Exception as e:
            exceptionCtrl.StoreException('ShoppingCart.cartOperation',{'operation':operation},str(e))
            response = jsonify({"error":"ERROR"})
            response.status_code = 500
            return response
        return response
    def checkout(self,cart_id):
        try:
            cart = db.cart.find_one({'_id':ObjectId(cart_id)})

        except Exception as e:
            exceptionCtrl.StoreException('ShoppingCart.checkout',{'cart_id':cart_id,'cart':cart},str(e))
            response = jsonify({"error":"ERROR"})
            response.status_code = 500
            return response
        return response

class CartAPI(MethodView):
    def get(self,cart_id):
        try:
            cart = db.cart.find_one({'_id':ObjectId(cart_id)})
            cart['_id']=str(cart['_id'])
            return jsonify(cart)
        except Exception as e:
            exceptionCtrl.StoreException('CartAPI.get',cart_id,str(e))
            response = jsonify({"error":"ERROR"})
            response.status_code = 500
            return response

    def post(self):
        try:
            shoppingCart = ShoppingCart()
            data = request.get_json(force=True)
            user = None
            status = True
            response = jsonify({})
            response.status_code=200
            permission, user = interceptor.headerInterceptor(request.headers,[])
            #First check if exist carts session
            if not 'cart_id' in data or not data['cart_id']:
                # Create the session
                status,cart_id = shoppingCart.startCartSession(user,data['shippingCity'])
            else:
                cart_id = data['cart_id']
            if status:
                if 'cartOperation' in data:
                    response = shoppingCart.cartOperation(cart_id,data['cartOperation'],data['operator'])
            else:
                return cart_id
        except Exception as e:
            exceptionCtrl.StoreException('CartAPI.post',data,str(e))
            response = jsonify({"error":"Parece que algo falló, estamos revisando, intentalo de nuevo por favor"})
            response.status_code = 500
            return response
        return response

class SetOrderDelivery(MethodView):
    def post(self):
        data = request.get_json(force=True)
        response = jsonify({})
        if 'cart_id' in data:
            if 'date' in data:
                db.cart.update({'_id':ObjectId(data['cart_id'])},{'$set':{'delivery_date':data['date']}})
                return response
        response = jsonify({"error":"No se pudo asignar la fecha de entrega, inténta nuevamente"})
        response.status_code=400
        return response
