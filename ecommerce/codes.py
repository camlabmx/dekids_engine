from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
import datetime as dt
from system import exceptionCtrl
from config import debug

class ApplyCodeAPI(MethodView):
    def loadAmount(self,items):
        amount = 0.0
        for item in items:
            amount += float(item['amount'])
        return amount

    def processAction(self,code_str,cart_session,action,item):
        if cart_session:
            discount = {}
            if action['type']=='percent':
                discount['type']='percent'
                discount['percent']=action['percent']
                discount['amount']=round(float(cart_session['subtotal'])*float((float(action['percent'])/100)),2)
                cart_session['discount']=discount
            if action['type']=='discount':
                discount['type']='discount'
                discount['amount']=action['discount']
                cart_session['discount']=discount
            #Update cart session
            cart_session['amount'] -= discount['amount']
            cart_session['applied_code']=code_str
            db.cart.update({'_id':cart_session['_id']},{'$set':cart_session})
            cart_session['_id']=str(cart_session['_id'])
            return cart_session
        if item:
            discount={}
            if action['type']=='percent':
                discount['type']='percent'
                discount['percent']=action['percent']
                discount['amount']=float(item['subtotal'])*float(float(action['percent'])/100)
                item['amount']=float(item['subtotal'])-discount['amount']
                item['discount']=discount
            if action['type']=='discount':
                discount['type']='discount'
                discount['amount']=action['discount']
                item['amount']=float(item['subtotal'])-discount['amount']
                item['discount']=discount
            if action['type']=='2x1':
                discount['type']='2x1'
                item['quantity']=int(item['quantity'])*2
                item['discount']=discount
            return item

    def processRule(self,code_str,cart_session,rule,action):
        try:
            # if rule['type']=='user':
            #     #Verify if username is on code's rule
            #     if cart_session['user']['username'] in rule['users']:
            #         # It fits the rule, lets check if it has more rules
            #         if 'rule' in rule:
            #             self.processRule(cart_session,rule['rule'],action)
            #         else:
            #             #Apply codes action
            #             for item in cart_session['cart']:
            #                 item = self.processAction(cart_session,action,item)
            #             cart_session['_id']=str(cart_session['_id'])
            #             response = jsonify({"cart":cart_session})
            #             response.status_code=200
            #             response.statusText = "Código Aplicado"
            #     else:
            #         response = jsonify({"error":"El usuario no es válido para el uso del código"})
            #         response.status_code = 400
            if rule['type']=='product':
                #Iterate over cart items and apply if it's on code's rule
                update = False
                for item in cart_session['cart']:
                    product = db.products.find_one({'_id':ObjectId(item['product'])})
                    if product['name'] in rule['products']:
                        update=True
                        item = self.processAction(code_str,None,action,item)
                        cart_session['amount'] = self.loadAmount(cart_session['cart'])
                if update:
                    cart_session['applied_code']=code_str
                    db.cart.update({'_id':cart_session['_id']},{'$set':cart_session})
                cart_session['_id']=str(cart_session['_id'])
                response = jsonify({"cart":cart_session})
                response.status_code=200
                response.statusText = "Código Aplicado"
            # if rule['type']=='group':
            #     #Check cart_session's user, verify if it's groups member
            #     pass
            # if rule['type']=='first':
            #     #Verify if is user's first buy
            #     pass
            if rule['type']=='amount':
                #Check total cart's amount, compare with code rule's amount
                if cart_session['amount']>=rule['amount']:
                    cart_session_updated = self.processAction(code_str,cart_session,action,None)
                    response = jsonify({"cart":cart_session_updated})
                    response.status_code=200
                    response.statusText = "Código Aplicado"
                else:
                    response = jsonify({"error":'No cumple con el monto requerido'})
                    response.status_code=400
                    response.statusText = "No cumple con el monto requerido"
            if rule['type']=='all':
                #Apply to all
                cart_session_updated = self.processAction(code_str,cart_session,action,None)
                response = jsonify({"cart":cart_session_updated})
                response.status_code=200
                response.statusText = "Código Aplicado"
            if rule['type']=='costco':
                #Look for rule's product
                sig = str(rule['days'])+'DSIG'
                green = str(rule['days'])+'DGRE'
                # Look for detox program in cart
                for item in cart_session['cart']:
                    if item['variantCode']==sig or item['variantCode']==green:
                        # Apply action
                        item = self.processAction(code_str,None,action,item)
                        # Then calculate final amount
                        cart_session['amount'] = self.loadAmount(cart_session['cart'])
                        cart_session['applied_code']='costco'
                        db.cart.update({'_id':cart_session['_id']},{'$set':cart_session})
                        cart_session['_id']=str(cart_session['_id'])
                        response = jsonify({"cart":cart_session})
                        response.status_code=200
                        response.statusText = "Código Aplicado"
                    # else:
                    #     response = jsonify({})
                    #     response.status_code=400
                    #     response.statusText = "El producto seleccionado no corresponde al código"
        except Exception as e:
            if debug:
                print(e)
            exceptionCtrl.StoreException('ApplyCodeAPI.processRule',{'cart_session':cart_session,'rule':rule,'action':action},str(e))
            response = jsonify({"error":"ERROR"})
            response.status_code = 500
            return response
        return response
    def post(self):
        try:
            response = jsonify({"error":"ERROR"})
            response.status_code = 500
            data = request.get_json(force=True)
            cart_id=data['cart_id']
            code_str=data['code']
            cart_session = db.cart.find_one({'_id':ObjectId(cart_id)})
            code = db.codes.find_one({'code':code_str})
            if cart_session and code:
                if code['redeemed']>0:
                    # code is still active
                    # Then lets make the validity check
                    if dt.datetime.now().strftime('%Y/%m/%d') >= code['validity']['start']:
                        # Now we're going to check if it has expiration date
                        if 'end' in code['validity']:
                            if dt.datetime.now().strftime('%Y/%m/%d') <= code['validity']['end']:
                                # self.processCode(cart_session,code)
                                response = self.processRule(code_str,cart_session,code['rule'],code['action'])
                            else:
                                response = jsonify({"error":"El código ha expirado"})
                                response.status_code = 400
                        else:
                            self.processRule(code_str,cart_session,code['rule'],code['action'])
                    else:
                        response = jsonify({"error":"El código aún no está activo"})
                        response.status_code = 400
                else:
                    # send error message
                    response = jsonify({"error":"El código ya ha sido usado"})
                    response.status_code = 400
        except Exception as e:
            exceptionCtrl.StoreException('ApplyCodeAPI.post',data,str(e))
            if debug:
                print(e)
            response = jsonify({"error":"ERROR"})
            response.status_code = 500
            return response
        return response
