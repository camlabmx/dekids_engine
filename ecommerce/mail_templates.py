
head_string = '''
<head>
  <title>ARCANA</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <link rel="stylesheet" href="http://camlab.mx/arcana_demo/materialize.custom.css">
</head>
'''

def successfulPurchaseMail(cart):
    mail_string = '''
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">
        <html>
    '''
    mail_string+=head_string
    details_string=''
    for item in cart['cart']:
        is_detox = ''
        if 'start_detox' in item and item['start_detox']:
            is_detox='Inicio de Detox :'
        details_string+='''
        <p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">
          '''+item['productName']+''' / '''+item['variantShortDesc']+'''
          / Cant. '''+str(item['quantity'])+'''/ '''+str(float(item['subtotal'])*int(item['quantity']))+'''
        </p>'''
    subtotal_string=''
    if 'subtotal' in cart:
        subtotal_string+='''<p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Subtotal: $'''+str(cart['subtotal'])+'''</p>'''
    discount_string = ''
    if 'discount' in cart:
        discount_string ='<p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Descuento: $'+str(int(cart['discount']['amount']))+'</p>'

    payment_string =''

    if cart['payment_type']=='card':
        payment_string+='<p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">*El cargo aparecerá en tu estados de cuenta a nombre de PAGOFACIL.NET.</p>'

    shipping_string='<p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:600;color:#636363;margin-top:0;margin-bottom:20px;">DATOS DE ENVÍO<p><p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Fecha de compra: '+cart['checkout_datetime']+'<p>'
    if cart['shippingCity']['category']=='AERO':
        shipping_string+='''
            <p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Enviado
                      a: '''+cart['shippingCity']['name']+'''</p>
            <p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Nombre de
              Receptor1: </p>
        '''
        shipping_string+='''
            <p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Para referencia tu numero de orden es:
                  <span style="font-weight:600;">'''+cart['track_id']+'''</span></p>
        '''

    else:
        shipping_string+='''
            <p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Enviado
              a: '''+cart['address']['street']+''', Col. '''+cart['address']['street2']+''',
              C.P. '''+cart['address']['zip']+'''<p>

            <p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Ciudad: '''+cart['shippingCity']['name']+'''</p>

            <p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Entre calles: '''+cart['address']['reference']+'''</p>
        '''
        shipping_string+='''
            <p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Para referencia tu numero de orden es:
                  <span style="font-weight:600;">'''+cart['track_id']+'''</span></p>
        '''

    footer ='''
        <table bgcolor="#2f2f2f" border="0" cellpadding="30" cellspacing="0" class="ed-footer" width="100%">
        <tbody>
        <tr>
          <td><a class="ed-footer-link" href="mailto:hola@arcanafoods.com" style="padding:0 0 5px 0;margin:0;display:block;color:#9dcf47;text-decoration: none !important;font-weight: 600;">hola@arcanafoods.com</a>

            <p class="paragraph-footer telephone" style="padding:0 0 5px 0;margin:0;display:block;color:#ffffff;">01 800 00DETOX(33869)</p>

            <p class="paragraph-footer follow" style="padding:0 0 5px 0;margin:0;display:block;color:#ffffff;">Siguenos en:</p>

            <p class="icons-email"><a href="https://www.facebook.com/ElixirDetoxMexico"></a>&nbsp;&nbsp;<a href="https://twitter.com/elixirdetox"></a>​ &nbsp;<a href="http://instagram.com/elixirdetox/"></a>&nbsp;&nbsp;<a href="http://www.pinterest.com/ElixirDetoxMx/"></a>&nbsp;&nbsp;<a href="https://www.youtube.com/user/elixirdetoxmexico"></a></p>
          </td>
          <td align="right"><span class="sg-image" ><img alt="logo elixirdetox" class="slogan-footer" src="https://www.elixirdetox.com/assets/slogan_footer.png" /></span></td>
        </tr>
        </tbody>
      </table>
    '''
    name=''
    if 'name' in cart['user']:
        name=cart['user']['name']
    mail_string+='''
        <body>
            <table border="0" cellpadding="0" cellspacing="0" class="ed-table" leftmargin="0" marginheight="0" marginwidth="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0 auto;padding: 0;-webkit-font-smoothing: antialiased;background-color:#EBEBEB;" topmargin="0" width="100%">
              <tbody>
              <tr>
                <td>
                  <table bgcolor="#DD592A" border="0" cellpadding="30" cellspacing="0" class="ed-header" width="100%">
                    <tbody>
                    <tr>
                      <td align="left"><span class="sg-image"><img alt="logo elixirdetox" height="44" src="http://camlab.mx/arcana_demo/img/arcana.png" style="width: auto; height: 44px;" /></span></td>
                      <td align="right">
                        <span style="color: #fff; font-size: 18px;">CONFIRMACIÓN DE COMPRA</span>
                      </td>
                    </tr>
                    </tbody>
                  </table>

                  <table bgcolor="#ffffff" border="0" cellspacing="0" class="ed-title" style="padding: 0 30px 0 30px;" width="100%">
                    <tbody>
                    <tr>
                      <td>
                        <p class="paragraph" style="font-size: 18px;line-height:18px; font-weight:400;color:#636363;margin-top:20px;margin-bottom:20px;">¡Hola '''+name+'''!<br/> <br/>Agradecemos la confianza depositada en Arcana. Estarás recibiendo tu pedido en los próximos x días hábiles</p>
                      </td>
                    </tr>
                    </tbody>
                  </table>

                  <table bgcolor="#ffffff" border="0" cellspacing="0" class="ed-content" style="padding: 10px 30px 5px 30px;" topmargin="50" width="100%">
                    <tbody>
                    <tr>
                      <td>
                        <p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:600;color:#636363;margin-top:0;margin-bottom:20px;">DETALLES DE TU COMPRA</p>
                        '''+details_string+'''
                        '''+subtotal_string+'''
                        '''+discount_string+'''
                        <p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Total: $'''+str(float(cart['amount']))+''' (IVA incluído)<p>
                        '''+payment_string+'''
                        '''+shipping_string+'''
                        <p class="paragraph" style="font-size: 16px;line-height:18px;font-weight:400;color:#636363;margin-top:0;margin-bottom:20px;">Para cualquier duda u aclaración, no dudes en escribirnos a hola@arcanafoods.com o enviar WhattsApp al +(52) 1 55 8080 3410.</p>

                      </td>
                    </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              </tbody>
            </table>
          </body>
        </html>
    '''
    return mail_string
