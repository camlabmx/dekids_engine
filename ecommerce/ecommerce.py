from ecommerce import shipping,landing,utils,codes,cart,page_content,checkout

def AddUrls(app):

    main_slider_view = landing.MainSliderAPI.as_view('main_slider_api')
    app.add_url_rule('/main-slider/', defaults={'section': 0}, view_func=main_slider_view, methods=['GET',])
    app.add_url_rule('/main-slider/<int:section>', view_func=main_slider_view, methods=['GET',])

    city_view = utils.CityAPI.as_view('city_view')
    app.add_url_rule('/city/',view_func=city_view,methods=['GET',])

    codes_api = codes.ApplyCodeAPI.as_view('codes_api')
    app.add_url_rule('/apply_code/',view_func=codes_api,methods=['POST',])

    cart_api = cart.CartAPI.as_view('cart_api')
    app.add_url_rule('/cart/<string:cart_id>',view_func=cart_api,methods=['GET',])
    app.add_url_rule('/cart_operation/',view_func=cart_api,methods=['POST',])

    set_delivery = cart.SetOrderDelivery.as_view('set_delivery')
    app.add_url_rule('/set_delivery/',view_func=set_delivery,methods=['POST',])

    content_api = page_content.ContentAPI.as_view('content_api')
    app.add_url_rule('/content/<string:section>',view_func=content_api,methods=['GET',])
    app.add_url_rule('/blog/<int:_id>',defaults={'section':'blog'},view_func=content_api,methods=['GET',])

    checkout_api = checkout.CheckoutAPI.as_view('checkout_api')
    paypal_api = checkout.PaypalCheckoutAPI.as_view('paypal_api')
    app.add_url_rule('/cart_checkout/',view_func=checkout_api,methods=['POST',])
    app.add_url_rule('/paypal_checkout/<string:cart_id>/<string:p_type>/<string:payment_id>/<string:payer_id>/<string:status>/',view_func=paypal_api,methods=['POST',])

    quote_api = shipping.QuotationAPI.as_view('quote_api')
    app.add_url_rule('/get_quotes/<string:cart_id>',view_func=quote_api,methods=['GET',])
    app.add_url_rule('/quotation/',view_func=quote_api,methods=['POST',])

    setquote_api = shipping.SetQuoteAPI.as_view('setquote_api')
    app.add_url_rule('/set_quote/',view_func=setquote_api,methods=['POST',])

    return app
