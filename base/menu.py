from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
from middleware import interceptor

class MenuAPI(MethodView):
    def get(self, section):
        permission, user = interceptor.headerInterceptor(request.headers,[])
        if not permission:
            response = jsonify({})
            response.status_code = 403
            return response
        cursor = db.menu.find({"section":section,"parent":{'$exists':False}}).sort("order",1)
        result = []
        for p in cursor:
            p['_id']=str(p['_id'])
            if 'access' in p:
                if user['role']=='root':
                    result.append(p)
                else:
                    if 'permissions' in user:
                        if p['access'] in user['permissions']:
                            result.append(p)
            else:
                result.append(p)
        cursor = db.menu.find({"section":section,"parent":{'$exists':True}}).sort("order",1)
        sub = []
        for p in cursor:
            p['_id']=str(p['_id'])
            if 'access' in p:
                if user['role']=='root':
                    sub.append(p)
                else:
                    if 'permissions' in user:
                        if p['access'] in user['permissions']:
                            sub.append(p)
            else:
                sub.append(p)
        return jsonify({"menu":result,"submenu":sub})#JSONEncoder().encode(result)
