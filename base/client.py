from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
from middleware import interceptor
from system import exceptionCtrl
from ecommerce import shipping
import hashlib
import time
import config
import requests
import json
import sys

class UserAddressAPI(MethodView):
    def loadAddressObject(self,data,user):
        address=None
        if 'street' and 'street2' and 'street3' and 'zip' in data:
            # Lets create address document
            address = {}
            address['street']=data['street']
            address['street2']=data['street2']
            address['street3']=data['street3']
            address['zip']=data['zip']
            if 'state' in data:
                address['state']=data['state']
            if 'reference' in data:
                address['reference']=data['reference']
            if 'phone' in data:
                address['phone']=data['phone']
                if user:
                    db.users.update({'_id':ObjectId(user['_id'])},{'$set':{'phone':data['phone']}})
            if 'cell' in data:
                address['cell']=data['cell']
                if user:
                    db.users.update({'_id':ObjectId(user['_id'])},{'$set':{'cell':data['cell']}})
            # Get coords
            url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+str(address['street'])+','+str(address['street2'])+','+str(address['street3'])+','+str(address['zip'])
            resp = requests.get(url)
            resultcoords = json.loads(resp.text)
            print(resultcoords)
            if 'results' in resultcoords and len(resultcoords['results'])>0:
                resultcoords = resultcoords['results'][0]
                address['city']=resultcoords['address_components'][4]['short_name']
                address['coords']={'lat':resultcoords['geometry']['location']['lat'],'lng':resultcoords['geometry']['location']['lng']}
            # Check if is status update or new address
            # if 'is_new' in data and int(data['is_new']):
            address['status']=1
            print(address)
        return address

    def get(self):
        permission, user = interceptor.headerInterceptor(request.headers,[])
        address = []
        if user:
            if 'address' in user:
                address = user['address']
        return jsonify({"address":address})

    def post(self):
        try:
            # permission, user = interceptor.headerInterceptor(request.headers,[])
            user = None
            response = jsonify({})
            data= request.get_json(force=True)
            if not user or not user['role'] == 'client':
                if 'email' in data:
                    # First we need to check if there's a user with that email already
                    user = db.users.find_one({'username':data['email']})
                    if not user:
                        if 'name' and 'last_name' in data:
                            # Lets create a user object
                            user = {}
                            user['name']=data['name']
                            user['last_name']=data['last_name']
                            user['email']=data['email']
                            user['username']=data['email']
                            user['role']='client'
                            user['access_level']=0
                            # enable for user login
                            # hash = hashlib.sha256()
                            # hash.update(data['pwd'].encode('utf8'))
                            # user['pwd']=hash.hexdigest()
                            db.users.insert_one(user)
                            user = db.users.find_one({'username':data['email']})
                            user['_id']=str(user['_id'])
                            #Create a token
                            # h = hashlib.sha256()
                            # h.update(str(config.secure_key+str(time.time())).encode('utf8'))
                            # token=h.hexdigest()
                            # db.tokens.insert({'username':user['username'],'token':token,'expire':time.time()+1800})
                            #Then set cart's owner
                            address = self.loadAddressObject(data,None)
                            db.cart.update({'_id':ObjectId(data['cart_id'])},{'$set':{'user':user,'address':address}})

                        else:
                            response = jsonify({})
                            response.statusText='Información Incompleta'
                            response.status_code=400
                            return response
                    else:
                        # if 'no-login' in data and data['no-login']==1:
                        address = self.loadAddressObject(data,None)
                        if not address:
                            response = jsonify({})
                            response.statusText='Información Incompleta'
                            response.status_code=400
                            return response
                        tmp_user = {"email":data['email'],"username":data['email']}
                        if 'name' in data and 'last_name' in data:
                            tmp_user['name']=data['name']
                            tmp_user['last_name']=data['last_name']
                        db.cart.update({'_id':ObjectId(data['cart_id'])},{'$set':{'user':tmp_user,"address":address}})
                        # quote shipping
                        quotes = shipping.quotation(data['cart_id'])
                        response = jsonify({"quotes":quotes})
                        return response
                        # response = jsonify({"error":"user"})
                        # return response

                else:
                    response = jsonify({})
                    response.statusText='Información Incompleta'
                    response.status_code=400
                    return response
            user['_id']=str(user['_id'])
            address = self.loadAddressObject(data,user)
            if address:
                user['address']=[]
                user['address'].append(address)
                db.users.update({'_id':ObjectId(user['_id'])},{'$set':{'address':user['address']}})
                if 'cart_id' in data:
                    # add shipping address to cart
                    db.cart.update({'_id':ObjectId(data['cart_id'])},{'$set':{'address':address}})
                    # quote shipping
                    quotes = shipping.quotation(data['cart_id'])
            return jsonify({'user':user,'quotes':quotes})
        except Exception as e:
            print(e)
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response

class OrderHistoryAPI(MethodView):
    def get(self):
        permission, user = interceptor.headerInterceptor(request.headers,[])
        try:
            response = jsonify({})
            if user:
                t_res = []
                o_res = []
                tracking = db.cart.find({"user.username":user['username'],"status":"checkout"})
                orders = db.cart.find({"user.username":user['username'],"status":"finished"})
                for t in tracking:
                    detail = ''
                    for item in t['cart']:
                        detail += item['productName']+', '
                    ship = db.shipping.find_one({"order":str(t['_id']),"status":"pending"})
                    if ship:
                        t_res.append({"track_id":t['track_id'],"detail":detail,"date":ship['delivery'],"total":t['amount']})
                    else:
                        t_res.append({"track_id":t['track_id'],"detail":detail,"total":t['amount']})
                for o in orders:
                    detail = ''
                    for item in o['cart']:
                        detail += item['productName']+', '
                    o_res.append({"track_id":o['track_id'],"detail":detail,"checkout_date":o['checkout_datetime'],"start_date":o['startTime'],"total":o['amount']})
                response = jsonify({"orders":o_res,"tracking":t_res})
            return response
        except Exception as e:
            exceptionCtrl.StoreException('client.OrderHistoryAPI',{'user':user,'stack_trace':str(sys.exc_info())},str(e))
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response
