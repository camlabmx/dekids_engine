from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db


class ProductsAPI(MethodView):

    def get(self, product_id):
        if product_id is None:
            cursor = db.products.find()
            result = []
            for p in cursor:
                p['_id']=str(p['_id'])
                result.append(p)
            return jsonify({"data":result})#JSONEncoder().encode(result)
        else:
            data = db.products.find_one({'_id':ObjectId(product_id)})
            data['_id']=str(data['_id'])
            return jsonify({"data":data})

    def post(self,product_id):
        product = {}
        data = request.get_json(force=True)
        if 'name' in data:
            product['name']=data['name']
        if 'tags' in data:
            product['tags']=data['tags']
        if product_id is None and '_id' not in data:
            res_id=db.products.insert(product)
            return jsonify({"Inserted":str(res_id)})
        else:
            product=db.products.update({'_id':ObjectId(data['_id'])},product)
            return jsonify({"Updated":str(product)})

class CartProductsAPI(MethodView):
    def get(self,section):
        try:
            query = {}
            if section:
                if not section=='all':
                    query['section']=section
                    cursor = db.products.find(query)
                else:
                    cursor = db.products.find(query,{'name':1,'short_name':1,'variants':1,'cities':1,'main_image':1,'section':1})
            result = []
            for p in cursor:
                p['_id']=str(p['_id'])
                result.append(p)
            return jsonify({"data":result})
        except Exception as e:
            print(e)
            return jsonify({"error":str(e)})
