from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
from middleware import interceptor
from system import exceptionCtrl
import hashlib
from auth import validate


class UserAPI(MethodView):
    def get(self,role):
        try:
            # import ipdb; ipdb.set_trace()
            # permission, user = interceptor.headerInterceptor(request.headers,[])
            # if not permission:
            #     response = jsonify({})
            #     response.status_code = 403
            #     return response
            user_list =[]
            if role:
                users = db.users.find({"role":role,"active":True},{'username':1,'name':1,'last_name':1,'role':1})
            else:
                users = db.users.find({"active":True},{'username':1,'name':1,'last_name':1})
            for user in users:
                user['_id']=str(user['_id'])
                user_list.append(user)
            return jsonify({"users":user_list})


        except Exception as e:
            exceptionCtrl.StoreException('user.UserAPI.get',{'role':role},str(e))
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response
    def post(self):
        try:
            data = request.get_json(force=True)
            if 'username' in data and 'pwd' in data and 'confirmation' in data:
                if 'role' in data and 'access_level' in data:
                    # Validate pwd & confirmation
                    if data['pwd']==data['confirmation']:
                        # We're going to check username
                        search_user = db.users.find_one({'username':data['username']})
                        if search_user:
                            response = jsonify({"error":"Usuario inválido"})
                            response.status_code = 400
                        else:
                            hash = hashlib.sha256()
                            hash.update(data['pwd'].encode('utf8'))
                            user = {}
                            user['username']=data['username']
                            user['email']=data['username']
                            user['pwd']=hash.hexdigest()
                            user['role']=data['role']
                            user['access_level']=data['access_level']
                            if 'name' in data and data['name']:
                                user['name']=data['name']
                            if 'last_name' in data and data['last_name']:
                                user['last_name']=data['last_name']
                            if 'gender' in data and data['gender']:
                                user['gender']=data['gender']
                            if 'birthday' in data and data['birthday']:
                                user['birthday']=data['birthday']
                            # Lets create the user
                            db.users.insert(user)

                            token_api=validate.TokenizeAPI()
                            cart_id = None
                            if 'cart_id' in data:
                                cart_id=data['cart_id']
                            token,logged,redirect = token_api.validate(user['username'],data['pwd'],cart_id)
                            response= jsonify({"token":token,"user":logged})
                            jsonify.status_code=200
                    else:
                        response = jsonify({"error":"La contraseña no coincide"})
                        response.status_code = 400
                else:
                    response = jsonify({"error":"Información incompleta"})
                    response.status_code = 400
            else:
                response = jsonify({"error":"Información incompleta"})
                response.status_code = 400
            return response
        except Exception as e:
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response
