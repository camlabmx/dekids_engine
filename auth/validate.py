from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
import hashlib
import time
import config

class TokenizeAPI(MethodView):

    def validate(self,username,pwd,cart_id=None):
        try:
            # Lets get users data
            user = db.users.find_one({"username":username})
            if user:
                # Lets validate pwd
                if self.check_pwd(user,pwd):
                    #Create and store the Token
                    h = hashlib.sha256()
                    h.update(str(config.secure_key+str(time.time())).encode('utf8'))
                    token=h.hexdigest()
                    # Remove previous tokens
                    old_token=db.tokens.find_one({'username':username})
                    if old_token:
                        db.tokens.remove({'_id':old_token['_id']})
                    db.tokens.insert({'username':username,'token':token,'expire':time.time()+1800})
                    if cart_id:
                        #Then set carts user
                        user['_id']=str(user['_id'])
                        db.cart.update({"_id":ObjectId(cart_id)},{"$set":{"user":user}})

                    #Then create frontend local storage user object
                    logged = {}
                    logged['username']=user['username']
                    logged['role']=user['role']
                    logged['access_level']=user['access_level']
                    redirect=''
                    if 'name' in user:
                        logged['name']=user['name']
                    if 'last_name' in user:
                        logged['last_name']=user['last_name']
                    if user['role']=='delivery':
                        redirect='delivery'
                    if user['role']=='admin':
                        redirect='dashboard'
                    if user['role']=='client':
                        redirect='cart'
                    return token,logged,redirect
            return False,False,None
        except Exception as e:
            print(str(e))
            return False,False,None

    def check_pwd(self,user,pwd):
        try:
            # Lets check hashed pwd with stored in user
            hash = hashlib.sha256()
            hash.update(pwd.encode('utf8'))
            if hash.hexdigest()==user['pwd']:
                return True
            return False
        except Exception as e:
            return False

    def post(self):
        try:
            data = request.get_json(force=True)
            if 'username' in data and 'pwd' in data:
                cart_id = None
                if 'cart_id' in data:
                    cart_id = data['cart_id']
                token,logged,redirect = self.validate(data['username'],data['pwd'],cart_id)
                if token:
                    return jsonify({"token":token,"user":logged,"redirect":redirect})
            response = jsonify({"error":"Credenciales inválidas"})
            response.status_code=400
            return response
        except Exception as e:
            response = jsonify({"error":str(e)})
            response.status_code=500
            return response

class FbLogin(MethodView):
    def post(self):
        data = request.get_json(force=True)
        try:
            response = data['response']
            fb_data = data['data']
            cart_id=data['cart_id']
            if response['status']=='connected':
                #Check if theres already user's object
                if not db.users.find_one({'fb_id':fb_data['id']}):
                    #Create Object
                    fb_user = {
                        "fb_id":fb_data['id'],
                        "name":fb_data['name'],
                        "email":fb_data['email'],
                        "username":fb_data['email'],
                        "role":'client'
                        }
                    db.users.insert(fb_user)
                db.tokens.insert({'username':fb_data['id'],'token':response['authResponse']['accessToken'],'expire':response['authResponse']['expiresIn']})
                user = db.users.find_one({'fb_id':fb_data['id']})
                if cart_id:
                    #Then set carts user
                    user['_id']=str(user['_id'])
                    db.cart.update({"_id":ObjectId(cart_id)},{"$set":{"user":user}})
            return jsonify({})
        except Exception as e:
            print(e)
            response = jsonify({"error":str(e)})
            response.status_code=500
            return response
