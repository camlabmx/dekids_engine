from auth import user,validate

def AddUrls(app):
    token_api = validate.TokenizeAPI.as_view('token_api')
    app.add_url_rule('/tokenize/', view_func=token_api, methods=['POST'])

    fb_login = validate.FbLogin.as_view('fb_login')
    app.add_url_rule('/fb_login/', view_func=fb_login, methods=['POST'])

    user_api = user.UserAPI.as_view('user_api')
    app.add_url_rule('/user/',defaults={'role':None} ,view_func=user_api, methods=['GET'])
    app.add_url_rule('/user/<string:role>' ,view_func=user_api, methods=['GET'])
    app.add_url_rule('/user/', view_func=user_api, methods=['POST'])
    return app
