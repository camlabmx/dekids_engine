from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
from middleware import interceptor
from system import exceptionCtrl
from database import db
import datetime as dt

class BasicAnalysisAPi(MethodView):
    def post(self):
        permission, user = interceptor.headerInterceptor(request.headers,['view_basic_analysis'])
        if not permission:
            response = jsonify({})
            response.status_code = 403
            return response
        try:
            try:
                data = request.get_json(force=True)
            except:
                data = {}
            # Look for date range
            from_date = None
            to_date = None

            if 'from' in data:
                from_date = data['from']
            if 'to' in data:
                to_date = data['to']
            res = {
                "amount":0,
                "subtotal":0,
                "orders":0,
            }
            if not from_date:
                from_date = '01'+dt.datetime.now().strftime('/%m/%Y')
            if not to_date:
                to_date = dt.datetime.now().strftime('%d/%m/%Y')
            orders = db.cart.find({'checkout_datetime':{'$gte':from_date},'checkout_datetime':{'$lte':to_date}})
            for order in orders:
                res['subtotal']+=order['subtotal']
                res['amount']+=order['amount']
                res['orders']+=1
            return jsonify(res)
        except Exception as e:
            print(e)
            exceptionCtrl.StoreException('orders.OrdersAPI.post',{'operation':'query'},str(e))
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response
