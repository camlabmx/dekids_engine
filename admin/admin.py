from admin import shipping,orders,inventory,reports

def AddUrls(app):
    orders_api = orders.OrdersAPI.as_view('orders_api')
    app.add_url_rule('/query_orders/',view_func=orders_api,methods=['POST',])

    shipping_api = shipping.ShippingAPI.as_view('shipping_api')
    app.add_url_rule('/pending_shipping/',defaults={'status': 'daily'},view_func=shipping_api, methods=['GET',])
    app.add_url_rule('/shipping_operation/',view_func=shipping_api, methods=['POST',])

    create_routes = shipping.CreateRoutesAPI.as_view('create_routes')
    app.add_url_rule('/evolve_routes/<int:routes>',view_func=create_routes,methods=['GET'])

    basic_analysis = reports.BasicAnalysisAPi.as_view('basic_analysis')
    app.add_url_rule('/basic_analysis/',view_func=basic_analysis,methods=['POST'])

    routes_api = shipping.RoutesAPI.as_view('routes_api')
    app.add_url_rule('/routes/',defaults={'status': 'today'},view_func=routes_api, methods=['GET',])
    app.add_url_rule('/routes/<string:status>',view_func=routes_api, methods=['GET',])
    app.add_url_rule('/route_by_owner/',defaults={'status': 'owner'},view_func=routes_api, methods=['GET',])
    app.add_url_rule('/assign_delivery/',defaults={'operation_type': 'in_charge'},view_func=routes_api, methods=['POST',])

    inventory_api = inventory.InventoryAPI.as_view('inventory_api')
    app.add_url_rule('/get_inventory/',view_func=inventory_api, methods=['POST',])

    return app
