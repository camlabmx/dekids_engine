from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
from middleware import interceptor
from system import exceptionCtrl
import datetime as dt
from config import page_size

class InventoryAPI(MethodView):
    def post(self):
        permission, user = interceptor.headerInterceptor(request.headers,['get_inventory'])
        if not permission:
            response = jsonify({})
            response.status_code = 403
            return response
        try:
            data = request.get_json(force=True)
            query = {}
            res = []
            if 'query' in data:
                query = data['query']
            inventory = db.inventory.find(query)
            # Change ObjectId for string index
            for obj in inventory:
                obj['_id']=str(obj['_id'])
                res.append(obj)
            return jsonify({'data':res})
        except Exception as e:
            print(e)
            exceptionCtrl.StoreException('inventory.InventoryAPI.get',{},str(e))
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response
