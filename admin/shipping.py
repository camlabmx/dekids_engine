from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
from middleware import interceptor
from system import exceptionCtrl
from database import db
import datetime as dt
import json,requests
import random
import sys

def InitPopulation(individuals,routes,points):
    population = []
    available = []
    for i in range(0,points):
        available.append(i)
    for i in range(0,individuals):
        individual = []
        tmp = []
        tmp = tmp+available
        for x in range(0,routes):
            individual.append([])
        while tmp:
            selected = tmp[random.randint(0,len(tmp)-1)]
            individual[random.randint(0,routes-1)].append(selected)
            tmp.remove(selected)
        population.append(individual)
        print("ind:"+str(individual))
    return population

def Fitness(population,coords):
    fitness = {}
    fitness_list = []
    for individual in population:
        fit=0
        for route in individual:
            #Append Elixir base coords to every route

            for i in range(0,len(route)-1):
                x1=coords[route[i]]['lat']
                y1=coords[route[i]]['lng']
                x2=coords[route[i+1]]['lat']
                y2=coords[route[i+1]]['lng']
                fit = fit+(((x2-x1)**2)+((y2-y1)**2))**0.5
        fitness[fit]=individual
        fitness_list.append(fit)
    return fitness,fitness_list

def Selection(fitness,fitness_list,individuals):
    #Order fitness_list
    fitness_list.sort()
    next_gen = []
    for i in range(0,individuals):
        next_gen.append(fitness[fitness_list[i]])
    return next_gen

def Mutate(individual,mut_percent):
    new_ind = individual[:]
    contr = 0
    for route in individual:
        for point in route:
            if random.randint(0,100)<mut_percent:
                # new_ind[contr].remove(point)
                left=new_ind[contr][:new_ind[contr].index(point)]
                right=new_ind[contr][new_ind[contr].index(point)+1:]
                new_ind[contr] = left+right
                mut_route = random.randint(0,len(individual)-1)
                if len(new_ind[mut_route])>1:
                    insert_in = random.randint(0,len(new_ind[mut_route])-1)
                    sec1 = new_ind[mut_route][:insert_in]
                    sec1.append(point)
                    sec2 = new_ind[mut_route][insert_in:]
                    new_ind[mut_route] = sec1+sec2
                else:
                    new_ind[mut_route].append(point)
                return new_ind
        contr=contr+1
    print("Mut: "+str(new_ind))
    return new_ind

def Cross(population,individuals,cross_percent,mut_percent):
    next_gen = []
    for i in range(0,int(individuals/2)):
        ind1 = population[random.randint(0,len(population)-1)][:]
        ind2 = population[random.randint(0,len(population)-1)][:]
        n1 = Mutate(ind1[:],mut_percent)
        n2 = Mutate(ind2[:],mut_percent)
        next_gen.append(n1)
        next_gen.append(n2)
    return next_gen

def EvolveRoutes(delivery_men):
    try:
        #vars
        generations = 250
        individuals = 40
        cross_percent = 20
        mut_percent = 0
        selection_percent = 20
        fitness={}
        fitness_list=[]
        # Get all day shipping objects
        date = dt.datetime.now().strftime('%d/%m/%Y')
        # date = '29/05/2017'
        shipping = db.shipping.find({"delivery":date})
        coords = []
        for point in shipping:
            append_point=True
            # print("Processing:"+str(point['_id']))
            order = db.cart.find_one({'_id':ObjectId(point['order'])})
            if not order:
                print("Missing order object:"+str(point))
            else:
                if 'address' in order and order['address'] and len(order['address'])>0:
                    point['address']=order['address']
                else:
                    print('ERROR: NO ADDRESS')
                    print(point)
                if not 'coords' in point['address'] or not point['address']['coords']:
                    # Get the coords
                    url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+str(point['address']['street'])+','+str(point['address']['street2'])+','+str(point['address']['zip'])
                    # url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+point['address']['street']
                    resp = requests.get(url)
                    data = json.loads(resp.text)
                    if 'results' in data and len(data['results'])>0:
                        data = data['results'][0]
                        point['address']['coords']={'lat':data['geometry']['location']['lat'],'lng':data['geometry']['location']['lng']}
                        db.cart.update({"_id":ObjectId(point['order'])},{'$set':{'address':point['address']}})
                    else:
                        append_point=False
                        #print("Order:"+str(point['order'])+'|'+point['address']['street']+','+point['address']['street2']+','+point['address']['zip'])
                        print("No location")
                if append_point:
                    # print("Appending:"+str(point['_id']))
                    coords.append({'lat':point['address']['coords']['lat'],'lng':point['address']['coords']['lng'],'shipping':str(point['_id'])})
                    # print("Done")
        population = InitPopulation(individuals,delivery_men,len(coords))
        # print("InitPopulation")
        # print(coords)
        for generation in range(0,generations):
            print("GEN "+str(generation+1)+":")
            fitness,fitness_list = Fitness(population,coords)
            # print("FITNESS:")
            fitness_list.sort()
            # print(fitness_list)
            next_gen = []
            next_gen = next_gen+Selection(fitness,fitness_list,int((selection_percent*0.01)*individuals))
            next_gen = next_gen+Cross(population,individuals-len(next_gen),cross_percent,mut_percent)
            population = next_gen
            # print("BEST:")
            # print(fitness_list)
            # print(fitness[fitness_list[0]])
        result = []
        # Set final route data
        for route in fitness[fitness_list[0]]:
            route_coords = []
            for point in route:
                route_coords.append(coords[point]['shipping'])
                db.shipping.update({'_id':ObjectId(coords[point]['shipping'])},{'$set':{'status':'on_route'}})
            # Lets check distance and time
            distance = 0
            expected_time = 0
            #Insert to db
            db.routes.insert({'date':date,'points':route_coords})
        return True
    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        print(exc_type, exc_tb.tb_lineno)
        print(sys.exc_info())
        exceptionCtrl.StoreException('shipping.EvolveRoutes',{'delivery_men':delivery_men,'stack_trace':str(sys.exc_info())},str(e))
        return False

def getRoutes():
    # get stored routes
    date = dt.datetime.now().strftime('%d/%m/%Y')
    # date = '30/10/2017'
    daily_route = db.routes.find({'date':date})
    shipping = db.shipping.find({"delivery":date})
    # Order shipping data and set in points
    shipping_data = {}
    for ship in shipping:
        # ship['status']='on_route'
        # db.shipping.update({'_id':ship['_id']},{'$set':{'status':'on_route'}})
        ship['_id']=str(ship['_id'])
        ship['order']=str(ship['order'])

        order = db.cart.find_one({'_id':ObjectId(ship['order'])})
        if order:
            ship['client']=order['user']
            ship['track_id']=order['track_id']
            if 'address' in order:
                ship['address']=order['address']
        shipping_data[ship['_id']]=ship

    res = []
    for route in daily_route:
        route['_id']=str(route['_id'])
        data_point = []
        for point in route['points']:
            data_point.append(shipping_data[point])
        route['points']=data_point
        res.append(route)
    return res

class CreateRoutesAPI(MethodView):
    def get(self,routes):
        res=[]
        if EvolveRoutes(routes):
            res=getRoutes()
        return jsonify({"routes":res})

class RoutesAPI(MethodView):
    def get(self,status):
        res = []
        date = dt.datetime.now().strftime('%d/%m/%Y')
        # date = '29/05/2017'
        routes = None
        if status=='today':
            return jsonify({"routes":getRoutes()})
        if status=='owner':
            permission, user = interceptor.headerInterceptor(request.headers,['view_own_routes'])
            if not permission:
                response = jsonify({})
                response.status_code = 403
                return response
            routes = db.routes.find({'date':date,'delivery_man':user['username']})
        if routes:
            for route in routes:
                route['_id']=str(route['_id'])
                if 'delivery_man' in route:
                    route['delivery_man'] = db.users.find_one({'username':route['delivery_man']},{'username':1,'name':1,'last_name':1})
                    route['delivery_man']['_id']=str(route['delivery_man']['_id'])
                data_point = []
                for point in route['points']:
                    ship_obj = db.shipping.find_one({'_id':ObjectId(point)})
                    ship_obj['_id']=str(ship_obj['_id'])
                    order = db.cart.find_one({'_id':ObjectId(ship_obj['order'])})
                    ship_obj['address']=order['address']
                    ship_obj['client']=order['user']
                    ship_obj['track_id']=order['track_id']
                    if 'payment_status' in order:
                        ship_obj['payment_status']=order['payment_status']
                    ship_obj['debt']=order['amount']
                    data_point.append(ship_obj)
                route['points']=data_point
                res.append(route)
        return jsonify({"routes":res})
    def post(self,operation_type):
        try:
            # permission, user = interceptor.headerInterceptor(request.headers,['edit_routes'])
            # if not permission:
            #     response = jsonify({})
            #     response.status_code = 403
            #     return response
            data = request.get_json(force=True)
            if operation_type and operation_type=='in_charge':
                if '_id' in data:
                    if 'delivery_man' in data:
                        db.routes.update({'_id':ObjectId(data['_id'])},{'$set':{'delivery_man':data['delivery_man']['username']}})
            return jsonify({})

        except Exception as e:
            exceptionCtrl.StoreException('user.UserAPI.get',{'role':role},str(e))
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response

class ShippingAPI(MethodView):
    def get(self,status):
        date = dt.datetime.now().strftime('%d/%m/%Y')
        shipping = None
        future = None
        if status=='daily':
            shipping = db.shipping.find({"delivery":{'$lte':date},"status":"pending"})
            future = db.shipping.find({"delivery":{'$gt':date},"status":"pending"})
        res = []
        res_future = []
        if shipping:
            for ship in shipping:
                order = db.cart.find_one({'_id':ObjectId(ship['order'])})
                if not order:
                    print(ship)
                else:
                    if 'address' in order and order['address'] and len(order['address'])>0:
                        ship['address']=order['address']
                    else:
                        print('ERROR: NO ADDRESS')
                        print(ship)
                    ship['_id']=str(ship['_id'])
                    # ship['order']={"_id":str(ship['order']),"trackId":order['track_id'],"client":order['user']['name']+' '+order['user']['last_name']}
                    if 'depend' in ship:
                        ship['depend']=str(ship['depend'])
                    res.append(ship)
        if future:
            for ship in future:
                order = db.cart.find_one({'_id':ObjectId(ship['order'])})
                if not order:
                    print(ship)
                else:
                    if 'address' in order and order['address'] and len(order['address'])>0:
                        ship['address']=order['address']
                    else:
                        print('ERROR: NO ADDRESS')
                        print(ship)
                    ship['_id']=str(ship['_id'])
                    ship['order']=str(ship['order'])
                    if 'depend' in ship:
                        ship['depend']=str(ship['depend'])
                    res_future.append(ship)
        # return jsonify({"shipping":len(res)})
        return jsonify({"shipping":res,"future_shipping":res_future})
    def post(self):
        data = request.get_json(force=True)
        date = dt.datetime.now().strftime('%d/%m/%Y %H:%M')
        if data['status']=='delivered':
            #Update shipping data
            db.shipping.update({'_id':ObjectId(data['_id'])},{'$set':{'status':'ok','delivered':date}})
        if data['status']=='fail':
            db.shipping.update({'_id':ObjectId(data['_id'])},{'$set':{'attempt':date,'alert':'fail','details':data['details']}})
        return jsonify({})
