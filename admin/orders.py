from flask.views import MethodView
from flask import jsonify,request
from bson import ObjectId
from database import db
from middleware import interceptor
from system import exceptionCtrl
import datetime as dt
import random
from config import page_size

def LoadInfo(cart):
    start_detox = None
    for item in cart:
        if item['start_detox']:
            start_detox = item['start_detox']
    return start_detox


class OrdersAPI(MethodView):
    def post(self):
        permission, user = interceptor.headerInterceptor(request.headers,['get_orders'])
        if not permission:
            response = jsonify({})
            response.status_code = 403
            return response
        try:
            data = request.get_json(force=True)
            query = {'status':'checkout'}
            if 'query' in data:
                query = data['query']
                if '_id' in query:
                    query['_id'] = ObjectId(query['_id'])
                    res = db.cart.find(query)
                else:
                    res = db.cart.find(query,{'_id':1,'cart':1,'user':1,'payment_status':1,'payment_type':1,'start_detox':1,'status':1,'track_id':1}).sort('start_detox',1)
            else:
                res = db.cart.find(query,{'_id':1,'cart':1,'user':1,'payment_status':1,'payment_type':1,'start_detox':1,'status':1,'track_id':1}).sort('start_detox',1)
            data = []
            for result in res:
                start_detox=None;
                result['_id']=str(result['_id'])
                # get user name, last_name, phone & cellphone
                if 'cart' in result:
                    start_detox = LoadInfo(result['cart'])
                if start_detox:
                    result['start_detox']=start_detox

                # if 'user' in result:
                #     user=db.users.find_one({'username':result['user']['username']},{'username':1,'email':1,'name':1,'last_name':1,'phone':1,'cell':1})
                #     if user and '_id' in user:
                #         result['user']=user
                #         result['user']['_id']=str(result['user']['_id'])
                #     else:
                #         result['user'] = {'email':result['user']}
                shipping = db.shipping.find({'order':result['_id']})
                res_ship=[]
                for ship in shipping:
                    res_ship.append({'_id':str(ship['_id']),'status':ship['status'],'delivery':ship['delivery'],'ship':ship['ship']})
                result['shipping'] = res_ship
                data.append(result)
            return jsonify({'orders':data})
        except Exception as e:
            print(e)
            exceptionCtrl.StoreException('orders.OrdersAPI.post',{'operation':'query'},str(e))
            response = jsonify({'error':str(e)})
            response.status_code = 500
            return response
