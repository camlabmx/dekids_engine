#!flask/bin/python
from flask import Flask #, jsonify, abort, make_response, request, url_for
from flask_apscheduler import APScheduler

# from flask_restful import Api
from flask_cors import CORS#, cross_origin

from base import products,menu,client
from ecommerce import ecommerce
from auth import auth
from admin import admin

class Config(object):
    JOBS = [
        {
            'id': 'job1',
            'func': 'system.shipping:ShippifyService',
            'args': (),
            # 'trigger': 'interval',
            # 'seconds': 60
            'trigger': 'cron',
            'hour': 9,
            'minute': 34
            #Envios martes y viernes
            # 'day_of_week': 1
            #
        },
        {
            'id': 'job2',
            'func': 'system.shipping:ShippifyService',
            'args': (),
            'trigger': 'cron',
            #Envíos martes y viernes
            'day_of_week': 4
            #
        }
    ]

    SCHEDULER_API_ENABLED = True

app = Flask(__name__)
CORS(app)

app= auth.AddUrls(app)

app = admin.AddUrls(app)

app = ecommerce.AddUrls(app)

products_view = products.ProductsAPI.as_view('products_api')
app.add_url_rule('/products/', defaults={'product_id': None},view_func=products_view, methods=['GET',])
app.add_url_rule('/products/', defaults={'product_id': None}, view_func=products_view, methods=['POST',])
app.add_url_rule('/products/<string:product_id>', view_func=products_view,methods=['GET', 'POST', 'DELETE'])

menu_view = menu.MenuAPI.as_view('menu_api')
app.add_url_rule('/menu/', defaults={'section': 'website'},view_func=menu_view, methods=['GET',])
app.add_url_rule('/menu/<string:section>',view_func=menu_view, methods=['GET',])


cart_products_view = products.CartProductsAPI.as_view('cart_products_api')
app.add_url_rule('/cart_products/<string:section>',view_func=cart_products_view, methods=['GET',])

client_address_api = client.UserAddressAPI.as_view('client_address_api')
app.add_url_rule('/address/',view_func=client_address_api, methods=['GET','POST',])

order_history_api = client.OrderHistoryAPI.as_view('order_history_api')
app.add_url_rule('/order_history/',view_func=order_history_api, methods=['GET',])

if __name__ == '__main__':
    app.config.from_object(Config())
    scheduler = APScheduler()
    scheduler.init_app(app)
    scheduler.start()
    app.run(debug=False,host='0.0.0.0',port='5000')
